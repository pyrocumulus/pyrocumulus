# -*- coding: utf-8 -*-

from .default import *

DATABASE = {'default': {'host': 'localhost',
                        'port': 27017,
                        'db': 'pyrounittest'}}
