.. Pyrocumulus documentation master file, created by
   sphinx-quickstart on Tue Nov 26 19:17:54 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


|pyrocumulus-logo|

.. |pyrocumulus-logo| image:: ./_static/pyrocumulus.png
    :width: 600px
    :alt: Easy restful APIs for mongoengine Documents


Easy restful web services with tornado and mongodb on python 3.

Write some models with mongomotor, create a new pyrocumulus RestApplication
instance and that's it. You're done. You've got a free restful API for your
documents.


Getting started
+++++++++++++++

Install
-------

Pyrocumulus may be installed using ``pip``.

.. code-block:: sh

    $ pip install pyrocumulus

That's all.


Usage
-----

Let's write a simple blog app.

.. code-block:: python

   from mongomotor import connect, Document
   from mongomotor.fields import StringField, ListField

   class Author(Document):
       name =

.. toctree::
   :maxdepth: 1

   intro/installation
   intro/usage
   intro/staticfiles
   intro/templates


Guides
++++++
.. toctree::
   :maxdepth: 1

   guides/restapi
   guides/mongoengine


Development
+++++++++++
.. toctree::
   :maxdepth: 1

   TODO
   CHANGELOG
   CONTRIBUTORS


Reference
+++++++++
.. toctree::
   :maxdepth: 1

   api/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
