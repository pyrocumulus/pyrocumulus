Serving static files
====================

Tornado already has a static file handler, which has a little improvement
in pyrocumulus. Using the :class:`pyrocumulus.web.handlers.StaticFileHandler`
you can set multiple directories to be used as root dir for static files
in your settings file.

To use it is quite simple. First you need to set the variables STATIC_URL and
STATIC_DIRS in your settings file.

.. code-block:: python

    STATIC_URL = '/static/'
    STATIC_DIRS = ['/home/me/somedir',]

Now, instantiate StaticApplication

.. code-block:: python

    from pyrocumulus.web.applications import StaticApplication

    staticapp = StaticApplication()

And, finally, put your staticapp in the APPLICATIONS settings variable

.. code-block:: python

    APPLICATIONS = ['myapp.web.staticapp', ..., ..., ...]
