Using templates
===============

Pyrocumulus has an simple request handler that uses
tornado's template system with a little improvement on
how to look for templates.

To use that, simply subclass :class:`pyrocumulus.web.handlers.TemplateHandler`.

.. code-block:: python

    from pyrocumulus.web.handlers import TemplateHandler


    class MyHandler(TemplateHandler):
        def get(self):
	    template = 'myfile.html'
	    extra_context = {'some': 'thing'}
	    self.render_template(template, extra_context)


:class:`pyrocumulus.web.handlers.TemplateHandler` makes use of the
tornado template system, so you can use all features of that in
your templates [#]_. It takes root dir from the TEMPLATE_DIRS settings
variable.


.. [#] Look the tornado.template docs `here
       <http://tornado.readthedocs.org/en/latest/template.html>`_
