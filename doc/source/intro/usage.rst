Tutorial
========

The main feature of pyrocumulus is automatic create restful APIs for
mongodb documents (using MongoMotor), so let't try it!

Creating a new project
++++++++++++++++++++++

First, create a new project using the `pyrocumulus` command in your
command line:

.. code-block:: sh

    $ pyrocumulus createproject ~/pyrotut
    $ cd ~/pyrotut


Writing your models with mongomotor
++++++++++++++++++++++++++++++++++++

In this tutorial, we'll use the same models used on mongomotor tutorial.
So, in the file ``pyrotut/models.py``, write these models:

.. code-block:: python

    from mongomotor import Document, EmbeddedDocument
    from mongomotor.fields import (StringField, ReferenceField, IntField,
                                   EmbeddedDocumentField, ListField)


    class User(Document):
        email = StringField(required=True)
	first_name = StringField(max_length=50)
	last_name = StringField(max_length=50)


    class Comment(EmbeddedDocument):
        content = StringField()
	name = StringField(max_length=120)


    class Post(Document):
	title = StringField(max_length=120, required=True)
	# reverse_delete_rule=CASCADE
	authors = ListField(ReferenceField(User, reverse_delete_rule=2))
	tags = ListField(StringField(max_length=30))
	comments = ListField(EmbeddedDocumentField(Comment))

	meta = {'allow_inheritance': True}


    class TextPost(Post):
	content = StringField()


    class ImagePost(Post):
	image_path = StringField()


    class LinkPost(Post):
	link_url = StringField()


Configuring the restful api
+++++++++++++++++++++++++++

Using the models we created earlier, lets create a
:class:`pyrocumulus.web.applications.RestApplication` instance.

In the file ``pyrotut/web.py``, write the following code:

.. code-block:: python

    from pyrocumulus.web.applications import RestApplication
    from pyrotut.models import User, TextPost, LinkPost, ImagePost

    simple_blog_api = RestApplication(User, TextPost, LinkPost, ImagePost)

And that's it. You have a restful API for your documents.


Setting up the configurations
+++++++++++++++++++++++++++++

Before we run our application, we need to set up some config variables.
So, in the file ``settings.py``, change your database settings and the
APPLICATIONS variable.

.. code-block:: python


    PROJECT_NAME = 'pyrotut'
    TORNADO_PORT = 8888

    DATABASE = {'default': {'HOST': 'localhost',
                            'PORT': 27017,
		            'NAME': 'pyrotut'}}

    # list of applications to be served by tornado
    APPLICATIONS = ['pyrotut.web.simple_blog_api',]



And that's it. We have a new project created. Now, let's try
out something.

Testing the API
+++++++++++++++

For testing, we'll use the package ``requests``, so, install it.

.. code-block:: sh

    $ pip install requests


Now, is time to test our api, but first, lets see which url were
mapped by our RestApplication.

.. code-block:: python

    >>> from pyrotut.web import application
    >>> application.urls
    [('/api/user/(.*)', ...),
     ('/api/textpost/comments/(.*)', ...),
     ('/api/textpost/(.*)', ...),
     ('/api/linkpost/comments/(.*)', ...),
     ('/api/linkpost/(.*)', ...),
     ('/api/imagepost/comments/(.*)', ...),
     ('/api/imagepost/(.*)', ...)]
    >>>

As we can see, :class:`pyrocumulus.web.applications.RestApplication`
creates a url for each one of our Documents, and URLs for its EmbeddedDocuments
too. Then, we can send HTTP requests to it and use our automatic api.

So, first, lets run a instance of the tornado web server and then open a
Python shell and test our application.

.. code-block:: sh

    $ python pyromanager.py runtorando --daemonize
    $ python

.. note::

   If you want to use the asyncio main loop, use the option ``--asyncio``
   of the ``runtornado`` command.

Now, inside the python shell, we can start sending requests to our application

.. code-block:: python

    >>> import requests
    >>> # base url for api
    >>> base_url = 'http://localhost:8888/api/'
    >>> user_url = base_url + 'user/'
    >>> post_url = base_url + 'textpost/'
    >>> comments_url = post_url + 'comments/'

First, create a user to be the posts' author

.. code-block:: python

    >>> user_email = 'user@email.com'
    >>> first_name = 'user'
    >>> last_name = 'niceGuy'
    >>> params = {'email': user_email,
    ...           'first_name': first_name,
    ...           'last_name': last_name}
    ...
    >>> response = requests.post(user_url, data=json.dumps(params))
    >>> response.connection.close()
    >>> user_id = response.json()['id']
    >>> user_id
    '52ba237a7c1c8452919d03f8'

Now, create a post using the user created earlier

.. code-block:: python

    >>> post_title = 'My First Post'
    >>> post_content = """Mussum ipsum cacilds, vidis litro abertis. """
    >>> params = {'title': post_title,
    ...           'content': post_content,
    ...           'tags': ['pyrotut', 'pyrocumulus', 'mussum'],
    ...           'authors': [user_id]}
    ...
    >>> response = requests.post(post_url, data=json.dumps(params))
    >>> response.connection.close()
    >>> post_id = response.json()['id']
    >>> post_id
    '52ba23be7c1c8452919d03f9'

And then, make a comment!

.. code-block:: python

    >>> comment_name = 'john doe'
    >>> comment_content = 'bla bla bla...'
    >>> query = {'parent_id': post_id}
    >>> body = {'name': comment_name,
    ...         'content': comment_content}
    ...
    >>> response = requests.post(comments_url, params=params,
    ...                          data=json.dumps(body))
    >>> response.connection.close()
    >>> response = requests.get(post_url, params={'id': post_id})
    >>> response.connection.close()
    post_content = response.json()['content']
    >>> print(post_content)
    Mussum ipsum cacilds, vidis litro abertis.
    >>> post_comments = response.json()['comments']
    >>> post_comments
    [{'name': 'john doe', 'content': 'bla bla bla...'}]


That's it. We created a user, a post and then sent a comment to that post.
When retrieving the post, we get the comments list together, because our
handler has as default ``depth`` the value 1. If you want only the comments
list, to paginate them, you can use:

.. code-block:: python

    >>> requests.get(comments_url, params={'parent_id': post_id,
    ... 			           'page': 1, 'max': 10})


An API with authentication
++++++++++++++++++++++++++

Now, lets put an authentication in our api  so only authenticated users can
create posts and users. We will do that changing the file ``pyrotut/web.py``
creating an api read-only to access the posts and one api with athentication
to create users and posts.

So, lets change the file ``pyrotut/web.py`` to something like this:

.. code-block:: python

    from pyrocumulus.web.applications import RestApplication
    from pyrotut.models import User, TextPost, LinkPost, ImagePost

    readonlyapi = RestApplication(TextPost, readonly=True)
    authapi = RestApplication(TextPost, User, auth=True, url_prefix='apiauth/')

Now lets change the APPLICATIONS variable in the file ``settings.py``:

.. code-block:: python

   APPLICATIONS = ['pyrotut.web.readonlyapi', 'pyrotut.web.authapi']

Our api is ready, but one thing is still missing: In order to access the auth
api we need an access key. We can create a token using the command ``authcli``
in our command line:

.. code-block:: sh

    $ python pyromanager.py authcli add_token --token-name=mytoken --perms=pyrotut.models.User:crud,pyrotut.models.TextPost:crud
    Token successfull added!
    b1b2db89-ac0d-4142-b348-dfeff02f192a

.. note::

   The parameter --perms receives the perms related to each document. The format
   is:

   document.full.qualified.name:perm, ..., ...

.. note::

   Store the token that is shown in the screen. Is not possible to recover
   it later!

Now, restart the tornado server and enter again in a python shell:

.. code-block:: sh

   $ python pyromanager.py runtornado --kill
   $ python pyromanager.py runtornado --daemonize
   $ python

And we already can test it.

.. code-block:: python

   >>> import requests
   >>> # primeiro as urls que usaremos
   >>> AUTH_BASE_URL = 'http://locahost:8888/apiauth/'
   >>> READONLY_BASE_URL = 'http://localhost:8888/api/'
   >>> AUTH_USER_URL = AUTH_BASE_URL + 'user/'
   >>> AUTH_POST_URL = AUTH_BASE_URL + 'textpost/'
   >>> RO_POST_URL = READONLY_BASE_URL + 'textpost/'

First lets create an users. Pay attention to the ``Authorization`` header
where we pass ``token`` that is the token  generated in the command line.

.. code-block:: python

   >>> user_params = {'email': 'user@useremail.com', 'first_name': 'zé',
   ...                'last_name': 'da silva'}
   >>> user_params = json.dumps(user_params)
   >>> headers = {
   ... 'Authorization': 'token: b1b2db89-ac0d-4142-b348-dfeff02f192a'}
   >>> response = requests.post(AUTH_USER_URL, data=user_params,
   ...                          headers=headers)
   >>> response.connection.close()
   >>> user_id = response.json()['id']

Now we'll create a post:

.. code-block:: python

   >>> post_title = 'My Other Post'
   >>> post_content = """Mussum ipsum cacilds, vidis litro abertis. """
   >>> post_params = {'authors': [user_id], 'title': post_title,
   ...                'content': post_content,
   ...                'tags': ['pyrocumulus', 'auth']}
   >>> post_params = json.dumps(post_params)
   >>> response = requests.post(AUTH_POST_URL, data=post_params,
   ...                          headers=headers)
   >>> response.connection.close()
   >>> post_id = response.json()['id']

And now lets see the created post using the read-only api:

.. code-block:: python

   >>> response = requests.get(RO_POST_URL, params={'id': post_id})
   >>> response.connection.close()
   >>> print(response.json()['content'])
   Mussum ipsum cacilds, vidis litro abertis.

Now lets try to create a post using an wrong key:

.. code-block:: python

   >>> post_params = {'authors': [user_id], 'title': post_title,
   ...                'content': post_content,
   ...                'tags': ['pyrocumulus', 'auth']}
   >>> post_params = json.dumps(post_params)
   >>> bad_headers = {
   ... 'Authorization': 'token: invalid-one'}

   >>> response = requests.post(AUTH_POST_URL, data=post_params,
   ...                          headers=headers)
   >>> response.connection.close()
   >>> response.status_code
   403

And finally lets try to create an user throught the read-only api:

.. code-block:: python

   >>> post_params = {'authors': [user_id], 'title': post_title,
   ...                'content': post_content,
   ...                'tags': ['pyrocumulus', 'auth']}
   >>> post_params = json.dumps(post_params)
   >>> response = requests.post(RO_POST_URL, data=post_params,
   ...                          headers=headers)
   >>> response.connection.close()
   >>> response.status_code
   405

That's it!
