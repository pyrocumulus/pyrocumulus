Install
=======

Pyrocumulus installation is straight-forward. You can install it using
pip or from sources.

.. note::

   In order to make things easy, you better create a new virtualenv
   (using python 3) to install pyrocumulus

Using pip
+++++++++

Simple like:

.. code-block:: sh

    $ pip install pyrocumulus

That's all.

From sources
++++++++++++

The code for Pyrocumulus is hosted on gitorious. So, clone it.

.. code-block:: sh

    $ git clone git@gitorious.org:pyrocumulus/pyrocumulus.git


Then, install all dependencies (including tests and docs dependencies);

.. code-block:: sh

    $ cd pyrocumulus
    $ pip install -r requirements_env.txt

And then, run tests.

.. code-block:: sh

    $ python setup.py test
    $ python setup.py test --test-suite=functional_tests
