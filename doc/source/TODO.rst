What's comming?
===============


* refactor on pyrocumulus.web.applications and
  pyrocumulus.web.handlers - it stinks
* improve EmbeddedDocument inserts and updates
* implement auth on rest api
* better commands help
* refactor functional tests
* improve url configuration
* make mongoengine work with motor (can it be done?)
* refactor converters

and ALWAYS improve docs. :)
