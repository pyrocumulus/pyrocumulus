web Package
===========

:mod:`applications` Module
--------------------------

.. automodule:: pyrocumulus.web.applications
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`handlers` Module
----------------------

.. automodule:: pyrocumulus.web.handlers
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`request_handlers` Module
------------------------------

.. automodule:: pyrocumulus.web.request_handlers
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`template` Module
----------------------

.. automodule:: pyrocumulus.web.template
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`urlmappers` Module
------------------------

.. automodule:: pyrocumulus.web.urlmappers
    :members:
    :undoc-members:
    :show-inheritance:

