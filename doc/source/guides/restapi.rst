Understandig the rest API
=========================

The main functionality of pyrocumulus is easily create rest apis based
on your mongoengine documents. In this section lets take closer look
on how it works [#]_.


How does it work?
+++++++++++++++++

This is quite simple. Pyrocumulus has a special request handler,
:class:`pyrocumulus.web.handlers.RestHandler`. It get a
mongoengine.Document as a parameter and creates a restful api
for it with optional CORS support.

To use that, you need to map an url to
:class:`pyrocumulus.web.handlers.RestHandler` and
associate it with a document. If your document has embedded documents, they
also need to be mapped to urls, but using the
:class:`pyrocumulus.web.handlers.EmbeddedDocumentHandler` class.
To easily url configuration, pyrocumulus has the
:class:`pyrocumulus.web.urlmappers.DocumentURLMapper` class, wich
creates all urls needed, including urls to EmbeddedDocuments.

After your urls are done, you need to create an `tornado.web.Application`
to use your urls. To package all that together, you can use the
:class:`pyrocumulus.web.applications.RestApplication` class. You just pass
your documents as paramenters to it and everything else is created for you.


RestHandler
+++++++++++

Lets start understading how
:class:`pyrocumulus.web.handlers.RestHandler` works. In order to use
that, you need to associate a mongoengine.Document to that handler
and map a URL to. Its done using the URLSpec object.
Something like this:

.. code-block::  python

    >>> from pyrocumulus.web.handlers import RestHandler
    >>> from myapp.models import MyDocument
    >>> from tornado.web import URLSpec
    >>> urlspec = URLSpec('/api/mydocument/(.*)', RestHandler, model=MyDocument)

When you map a URL to  :class:`pyrocumulus.web.handlers.RestHandler`,
you get a rest api for your documet, supporting the following operations:

* GET request to `YOUR_DOCUMENT_URL + object pk`  to retrieve specific object
  from database.

  .. code-block:: python

      >>> requests.get('/api/mydocument/?id=SOMEID')

* GET request to `YOUR_DOCUMENT_URL` to list objects. You can use
  the ``page`` and ``max`` params to paginate the result and the order_by
  param for ordering that.

  .. code-block:: python

      >>> params = {'page': 1, 'max': 10, 'order_by': 'some_field'}
      >>> requests.get('/api/mydocument/?page=2&max=10&order_by=-field')

* POST request to `YOUR_DOCUMENT_URL` to insert a new object into the database.

  .. code-block:: python

      >>> params = {'name': 'Some name', 'location': 'Somewhere'}
      >>> request.post('/api/mydocument/', data=json.dumps(params))

* PUT request to `YOUR_DOCUMENT_URL` to update and object. You must use a
  primary key in the request parameters.

  .. code-block:: python

      >>> query = {id='some-id'}
      >>> body = {'name': 'Some new name', }
      >>> request.post('/api/mydocument/', params=query,
      ...              data=json.dumps(body))

* DELETE request to `YOUR_DOCUMENT_URL` to delete an object from database.

  .. code-block:: python

      >>> params = {'id': 'a9982lsdfk34kd'}
      >>> requests.delete('/api/mydocument/', params=params)

If your document has embedded documents, they also need to be mapped, but using
the :class:`pyrocumulus.web.handlers.EmbeddedDocumentHandler` class.

.. code-block::  python

    >>> from pyrocumulus.web.handlers import EmbbeddeDocumentRestHandler
    >>> from myapp.models import MyDocument, MyEmbeddedDocument
    >>> from tornado.web import URLSpec
    >>> urlspec = URLSpec('/api/mydocument/embeddedfield/(.*)',
                           EmbeddedDocumentRestHandler,
                           model=MyEmbeddedDocument, parent_doc=MyDocument)

In order to facilitate that, pyrocumulus has a
:class:`pyrocumulus.web.urlmappers.DocumentURLMapper`, which creates
all URLSpecs to a Document, including its EmbeddedDocuments.


DocumentURLMapper
+++++++++++++++++

The :class:`pyrocumulus.web.urlmappers.DocumentURLMapper` class gets
a Document and a RequestHandler in its constructor. The instance of
:class:`pyrocumulus.web.urlmappers.DocumentURLMapper` then creates all
needed urls to your api works.

.. code-block:: python

    >>> from pyrocumulus.web.urlmappers import DocumentURLMapper
    >>> from pyrocumulus.web.handlers import RestHandler
    >>> from myapp.models import MyDocument
    >>> mapper = DocumentURLMapper(MyDocument, RestHandler)
    >>> urls = mapper.urls

You can use the optional arguments ``url_prefix`` and ``url_name_prefix`` to
control how urls and urls names are created.

Once we are done with our urls, we need to create a ``tornado.web.Application``
instance to be served by our tornado server. Like this:

.. code-block:: python

    >>> from tornado.web import Application
    >>> app = Application(urls)

RestApplication
+++++++++++++++

In the pyrocumulus tutorial we used the
:class:`pyrocumulus.web.application.RestApplication` class to create our main
application. Its a subclass from `tornado.web.Application` that receives a
list of models and creates url mappings for it. It receives an optional
argument ``prefix`` to be used as ``url_prefix`` and ``url_name_prefix`` on
DocumentURLMapper.

.. code-block:: python

    >>> from pyrocumulus.web.applications import RestApplication
    >>> app = RestApplication(MyDocument, prefix='/myapp')

This is how the restapi was created automaticlly. But one thing still needs
to be explanined. How the tornado server runs our application. This is done
by the :class:`pyrocumulus.commands.runtornado.RunTornadoCommand` command.

runtornado command
++++++++++++++++++

The `runtornado` command is invoked in the  command line as:

.. code-block:: sh

    $ python pyromanager.py runtornado

Its default behavrior is to take the application to run from
:func:`pyrocumulus.web.applications.get_main_application`, but you can use
the parameter --application to specify an application to be used.

That's it.

.. [#] For a better understanding on how RequestHadler works, you should
       read the tornado documentation on RequestHadler `here
       <http://www.tornadoweb.org/en/stable/web.html#request-handlers>`_
