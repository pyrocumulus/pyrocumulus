Instalação
==========

A instalação do pyrocumulus é bem comum. Você pode instalá-lo usando o pip
ou através do repositório git.

.. note::

   Para facilitar as coisas, é melhor criar um novo virtualenv
   (usando python 3) para a instalação do pyrocumulus.


Usando pip
++++++++++

Simplesmente:

.. code-block:: sh

    $ pip install pyrocumulus

E é isso.

Usando o repositório git
++++++++++++++++++++++++

O código do pyrocumulus está hospedado no gitlab. Então clone:

.. code-block:: sh

    $ git clone git@gitlab.com:pyrocumulus/pyrocumulus.git


Instale as dependências (incluindo dependências de testes e documentação):

.. code-block:: sh

    $ cd pyrocumulus
    $ pip install -r requirements_env.txt

E execute os testes:

.. code-block:: sh

    $ python setup.py test
