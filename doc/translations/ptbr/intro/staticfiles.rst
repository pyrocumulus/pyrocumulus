Servindo arquivos estáticos
===========================

O tornado já possui um handler para arquivos estáticos, que foi ligeiramente
melhorado no pyrocumulus. Usando
:class:`pyrocumulus.web.handlers.StaticFileHandler` você pode configurar
multiplos diretórios para serem usados como o diretório raiz para servir
arquivos estáticos.

O uso é bem simples. Primeiro você precisa configurar as variáveis STATIC_URL
e STATIC_DIRS no ser arquivo de configuração.

.. code-block:: python

    STATIC_URL = '/static/'
    STATIC_DIRS = ['/home/me/somedir',]

Agora crie uma instância de :class:`pyrocumulus.web.handler.StaticFileHandler`

.. code-block:: python

    from pyrocumulus.web.applications import StaticApplication

    staticapp = StaticApplication()

E finalmente coloque sua ``staticapp`` na variável APPLICATIONS na
sua configuração:

.. code-block:: python

    APPLICATIONS = ['myapp.web.staticapp', ..., ..., ...]
