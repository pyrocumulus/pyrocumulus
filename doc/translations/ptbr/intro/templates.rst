Usando templates
================

Pyrocumulus tem um request handler bem simples
que usa o sistema de templates do tornado com uma
ligeira melhoria em como procurar pelos templates.

Para usar isto, simplesmente crie uma subclasse de
:class:`pyrocumulus.web.handlers.TemplateHandler`:

.. code-block:: python

    from pyrocumulus.web.handlers import TemplateHandler


    class MyHandler(TemplateHandler):
        def get(self):
	    template = 'myfile.html'
	    extra_context = {'some': 'thing'}
	    self.render_template(template, extra_context)


:class:`pyrocumulus.web.handlers.TemplateHandler` vai procurar pelo
template nos diretórios listados na variável de configuração
``TEMPLATE_DIRS``.

Como :class:`pyrocumulus.web.handlers.TemplateHandler` faz uso do
sistema de templates do tornado, todas das funcionalidades deste
podem ser usadas no pyrocumulus também [#]_

.. [#] Dê uma olhada na documentação do tornado.template `aqui
       <http://tornado.readthedocs.org/en/latest/template.html>`_
