Tutorial
========

A principal funcionalidade do pyrocumulus é a criação automática de APIs
restful para os seus documentos do mongodb (usando MongoMotor), então
vamos explorar isso!


Criando um novo projeto
+++++++++++++++++++++++

Vamos criar um novo projeto usando o comando ``pyrocumulus`` na linha de
comando:

.. code-block:: sh

    $ pyrocumulus createproject ~/pyrotut
    $ cd ~/pyrotut


Escrevendo os modelos com o MongoMotor
+++++++++++++++++++++++++++++++++++++++

Neste tutorial usaremos os mesmos modelos usados no tutorial do mongomotor.
No arquivo ``pyrotut/models.py``, escreva os seguintes modelos:

.. code-block:: python

    from mongomotor import Document, EmbeddedDocument
    from mongomotor.fields import (StringField, ReferenceField, IntField,
                                   EmbeddedDocumentField, ListField)


    class User(Document):
        email = StringField(required=True)
	first_name = StringField(max_length=50)
	last_name = StringField(max_length=50)


    class Comment(EmbeddedDocument):
        content = StringField()
	name = StringField(max_length=120)


    class Post(Document):
	title = StringField(max_length=120, required=True)
	# reverse_delete_rule=CASCADE
	authors = ListField(ReferenceField(User, reverse_delete_rule=2))
	tags = ListField(StringField(max_length=30))
	comments = ListField(EmbeddedDocumentField(Comment))

	meta = {'allow_inheritance': True}


    class TextPost(Post):
	content = StringField()


    class ImagePost(Post):
	image_path = StringField()


    class LinkPost(Post):
	link_url = StringField()


Configurando a API restful
++++++++++++++++++++++++++

Usando os modelos que criamos anteriormente, vamos criar uma instância de
:class:`pyrocumulus.web.applications.RestApplication`.

No arquivo ``pyrotut/web.py``, escreva o seguinte código:

.. code-block:: python

    from pyrocumulus.web.applications import RestApplication
    from pyrotut.models import User, TextPost, LinkPost, ImagePost

    simple_blog_api = RestApplication(User, TextPost, LinkPost, ImagePost)

E é isso. Você tem uma API restful para os seus documentos.


Ajustando as configurações
++++++++++++++++++++++++++

Antes de rodarmos nossa aplicação precisamos ajustar algumas variáveis de
configuração. Então, no arquivo ``settings.py`` altere as configurações
de banco de dados e altere também a variável APPLICATIONS.

.. code-block:: python


    PROJECT_NAME = 'pyrotut'
    TORNADO_PORT = 8888

    DATABASE = {'default': {'HOST': 'localhost',
                            'PORT': 27017,
		            'NAME': 'pyrotut'}}

    # list of applications to be served by tornado
    APPLICATIONS = ['pyrotut.web.simple_blog_api',]



E é isso, já temos tudo pronto. Agora vamos testar a coisa:

Testando a API
++++++++++++++

Para testar nós usaremos o pacote ``requests``. Então, primeiro
instale-o.

.. code-block:: sh

    $ pip install requests

Agora é hora de testar nossa api, mas primeiro vamos ver quais urls
foram criadas pela nossa aplicação.


.. code-block:: python

    >>> from pyrotut.web import application
    >>> application.urls
    [('/api/user/(.*)', ...),
     ('/api/textpost/comments/(.*)', ...),
     ('/api/textpost/(.*)', ...),
     ('/api/linkpost/comments/(.*)', ...),
     ('/api/linkpost/(.*)', ...),
     ('/api/imagepost/comments/(.*)', ...),
     ('/api/imagepost/(.*)', ...)]
    >>>

Como podemos ver, :class:`pyrocumulus.web.applications.RestApplication` cria
uma url para cada um dos nossos documentos e para os embedded documents
também. Então, agora podemos enviar requisições HTTP e usar a nossa api.

Então, primeiramente vamos iniciar uma instância do servidor do tornado
e abrir uma sessão no shell do Python para testar a aplicação.

.. code-block:: sh

    $ python pyromanager.py runtorando --daemonize
    $ python

Agora, já no shell do python, podemos começar a enviar requisições.

.. code-block:: python

    >>> import requests
    >>> # base url for api
    >>> base_url = 'http://localhost:8888/api/'
    >>> user_url = base_url + 'user/'
    >>> post_url = base_url + 'textpost/'
    >>> comments_url = post_url + 'comments/'

Primeiro, vamos criar um usuário para se o autor dos posts.

.. code-block:: python

    >>> user_email = 'user@email.com'
    >>> first_name = 'user'
    >>> last_name = 'niceGuy'
    >>> params = {'email': user_email,
    ...           'first_name': first_name,
    ...           'last_name': last_name}
    ...
    >>> response = requests.post(user_url, params)
    >>> response.connection.close()
    >>> user_id = response.json()['id']
    >>> user_id
    '52ba237a7c1c8452919d03f8'

Agora, criamos um post usando o usuário criado anteriormente:

.. code-block:: python

    >>> post_title = 'My First Post'
    >>> post_content = """Mussum ipsum cacilds, vidis litro abertis. """
    >>> params = {'title': post_title,
    ...           'content': post_content,
    ...           'tags': ['pyrotut', 'pyrocumulus', 'mussum'],
    ...           'authors': [user_id]}
    ...
    >>> response = requests.post(post_url, params)
    >>> response.connection.close()
    >>> post_id = response.json()['id']
    >>> post_id
    '52ba23be7c1c8452919d03f9'

E por fim, fazemos um comentário!

.. code-block:: python

    >>> comment_name = 'john doe'
    >>> comment_content = 'bla bla bla...'
    >>> params = {'name': comment_name,
    ...           'content': comment_content,
    ...           'parent_id': post_id}
    ...
    >>> response = requests.post(comments_url, params)
    >>> response.connection.close()
    >>> response = requests.get(post_url, params={'id': post_id})
    >>> response.connection.close()
    post_content = response.json()['content']
    >>> print(post_content)
    Mussum ipsum cacilds, vidis litro abertis.
    >>> post_comments = response.json()['comments']
    >>> post_comments
    [{'name': 'john doe', 'content': 'bla bla bla...'}]


E é isso. Nós criamos um usuário, um post e depois enviamos um comentário
ao post. Quando nós recuperamos um post também recuperamos juntamente a
lista com os comentários, isso porque os comentários são ``embedded documents``
e nosso handler tem o valor padrão de ``depth`` como sendo 1. Se você quer
somente os comentários, para poder paginá-los, pode usar:

.. code-block:: python

    >>> requests.get(comments_url + 'list', params={'parent_id': post_id,
    ... 					    'page': 1, 'max': 10})


Uma API com autenticação
++++++++++++++++++++++++

Agora, vamos colocar uma autenticação na nossa api para que somente
alguém autenticado possa criar posts e usuários. Faremos isto alterando
o arquivo ``pyrotut/web.py``, criando uma api somente-leitura para o acesso
aos posts e uma api com autenticação para criar os posts e os usuários.

Então, vamos alterar o arquivo ``pyrotut/web.py`` para que fique assim:

.. code-block:: python

    from pyrocumulus.web.applications import RestApplication
    from pyrotut.models import User, TextPost, LinkPost, ImagePost

    readonlyapi = RestApplication(TextPost, readonly=True)
    authapi = RestApplication(TextPost, User, auth=True, url_prefix='apiauth/')

Agora vamos alterar a variável APPLICATIONS no arquivo ``settings.py``.
Ficará assim:

.. code-block:: python

   APPLICATIONS = ['pyrotut.web.readonlyapi', 'pyrotut.web.authapi']

Pronto, nossa api já está pronta, mas ainda falta uma coisa: Para podermos
acessar a api autenticada precisamos de um token de acesso. Podemos criar
um token usando o comando ``authcli`` na nossa linha de comando, assim:

.. code-block:: sh

    $ python pyromanager.py authcli add_token --token-name=mytoken --perms=pyrotut.models.User:rw,pyrotut.models.TextPost:rw
    Token successfull added!
    b1b2db89-ac0d-4142-b348-dfeff02f192a

.. note::

   O parâmetro --perms recebe as permissões relacionadas a cada documento. O formato é:
   document.full.qualified.name:perm, ..., ...

.. note::

   Guarde o token que é mostrado na tela. Não é possível recuperá-lo depois!


Agora, reinicie o servidor do tornado e entre novamente num shell de python:

.. code-block:: sh

   $ python pyromanager.py runtornado --kill
   $ python pyromanager.py runtornado --daemonize
   $ python

E já podemos testar.

.. code-block:: python

   >>> import requests
   >>> # primeiro as urls que usaremos
   >>> AUTH_BASE_URL = 'http://locahost:8888/apiauth/'
   >>> READONLY_BASE_URL = 'http://localhost:8888/api/'
   >>> AUTH_USER_URL = AUTH_BASE_URL + 'user/'
   >>> AUTH_POST_URL = AUTH_BASE_URL + 'textpost/'
   >>> RO_POST_URL = READONLY_BASE_URL + 'textpost/'

Primeiro vamos criar um usuário. Repare que o parâmetro api_key
é onde usamos o token gerado na linha de comando.

.. code-block:: python

   >>> user_params = {'api_key': 'b1b2db89-ac0d-4142-b348-dfeff02f192a',
   ...                'email': 'user@useremail.com', 'first_name': 'zé',
   ...                 'last_name': 'da silva'}
   >>> response = requests.post(AUTH_USER_URL, params=params)
   >>> response.connection.close()
   >>> user_id = response.json()['id']

Agora criaremos um post

.. code-block:: python

   >>> post_title = 'My Other Post'
   >>> post_content = """Mussum ipsum cacilds, vidis litro abertis. """
   >>> post_params = {'api_key': 'b1b2db89-ac0d-4142-b348-dfeff02f192a',
   ...                'authors': [user_id], 'title': post_title,
   ...                'content': post_content,
   ...                'tags': ['pyrocumulus', 'auth']}
   >>> response = requests.post(AUTH_POST_URL, params=params)
   >>> response.connection.close()
   >>> post_id = response.json()['id']

E agora vamos ver o post criado através da api somente-leitura

.. code-block:: python

   >>> response = requests.get(RO_POST_URL, params={'id': post_id})
   >>> response.connection.close()
   >>> print(response.json()['content'])
   Mussum ipsum cacilds, vidis litro abertis.

Agora vamos tentar criar um post com uma chave errada

.. code-block:: python

   >>> post_params = {'api_key': '123123sdfsdfsa',
   ...                'authors': [user_id], 'title': post_title,
   ...                'content': post_content,
   ...                'tags': ['pyrocumulus', 'auth']}
   >>> response = requests.post(AUTH_POST_URL, params=params)
   >>> response.connection.close()
   >>> response.status_code
   403

E por fim vamos tentar criar um usuário pela api somente leitura

.. code-block:: python

   >>> post_params = {'api_key': 'b1b2db89-ac0d-4142-b348-dfeff02f192a',
   ...                'authors': [user_id], 'title': post_title,
   ...                'content': post_content,
   ...                'tags': ['pyrocumulus', 'auth']}
   >>> response = requests.post(RO_POST_URL, params=params)
   >>> response.connection.close()
   >>> response.status_code
   405

É isso.
