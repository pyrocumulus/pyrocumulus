Changelog
=========

* 0.7.1

  * Corrected handlers operation inheritance.
  * Corrected list of embedded document list fields.

* 0.7

  * Updated to mongomotor 0.6.2
  * Corrected rest api interface. Now the request methods are used correctly.
  * Added auto docs for auto api

* 0.6

  * Removed jsonp support
  * Refactor on handlers operations. Now uses decorators.
  * Bugfixes

* 0.5

  * added auth
  * changed document relational mapper from mongoengine to mongomotor
  * refactor on urlmappers and applications to allow handlers config from
    settings.

* 0.4.5

  * adding fake join - something like ref__field

* 0.4.4

  * Correcting RestHandler behavior when using converters

* 0.4.3

  * Correcting file descriptors handling on runtornado.
  * Module pyrocumulus.web.request_handlers now is called
    pyrocumulus.web.handlers
  * Adding StaticApplication
  * Adding TemplateHandler

* 0.4.2.1

  * Corrected bug with tornado server daemonized on mac os x

* 0.4.2

  * Correcting bug with objects ordering

* 0.4.1

  * Adding ordering on objects list

* 0.4

  * Refactor on pyrocumulus.web.applications interface
  * Added test command

* 0.3.2.1

  * Correcting import that was causing errors on unittest

* 0.3.2

  * basic template support based on settings variable

* 0.3.1

  * basic cors support

* 0.3

  * docs updated
  * better commands help
  * added get_allowed_options to RestHandler
  * createproject command improved

* 0.3-b3

  * Change on DocumentURLMapper to receive a request handler as a arg
    to its constructor

* 0.3-b2

  * pyrocumulus.tornadoweb module now is pyrocumulus.web package

* 0.3-b1

  * added RestApplication to easy url configuration
  * Added support to ListField and EmbeddedDocument on RestHandler
  * change to make runtornado command get tornado port from settings
  * change PYROCUMULUS_SETTINGS_MODULE env var on base command
  * minor fixes

* 0.2

  * File descriptors close corrected on RunTornadoCommand
  * Changing commands from setupcommands module to commands package with
    a new options configuration scheme
  * Added pyromanager.py to run the commands from pyrocumulus.commands
    package
  * TornadoApplicationTestCase changed to use pyromanager.py
  * Added pyrocumulus script
  * Changed BaseRestHandler to return a dict on _list_object and to accept
    json_extra_params
  * Corrected treatment for lists on BaseConverter.sanitize_dict

* 0.1.1

  * Correction on setup.py

* 0.1 - First release
