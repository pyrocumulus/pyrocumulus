Entendendo a API rest
=====================

A principal funcionalidade do pyrocumulus é a criação de APIs restfull
baseadas em documentos definidos com o MongoMotor. Aqui vamos dar uma olhada
mais de perto para ver como isso funciona [#]_.

Como funciona?
++++++++++++++

É bem simples. O pyrocumulus tem um request handler especial, a classe
:class:`pyrocumulus.web.handlers.RestHandler`. Ela recebe uma
subclasse de mongomotor.Document como parâmetro e cria uma API restful
para o documento com suporte opcional a CORS.

Para usar este handler, é preciso mapear uma url à
:class:`pyrocumulus.web.handlers.RestHandler` e associar o documento que será
passado como parâmetro para o handler. Se o documento possui embedded documents
estes documentos também precisam ser mapeados, mas desta vez usando a classe
:class:`pyrocumulus.web.handlers.EmbeddedDocumentHandler`. Para facilitar
esta configuração de urls o pyrocumulus tem a classe
:class:`pyrocumulus.web.urlmappers.DocumentURLMapper` que cria todas as
urls necessárias, inclusive urls para embedded documents.

Depois que as urls estão prontas, precisamos criar uma instancia de
`tornado.web.application` que usará estas urls. Para empacotar todas estas
coisas juntas, o pyrocumulus tem a classe
:class:`pyrocumulus.web.applications.RestApplication`. Você simplesmente
passa seus documentos como parâmetros e todo o resto é criado para você!


RestHandler
+++++++++++

Vamos começar entendendo como a classe
:class:`pyrocumulus.web.handlers.RestHandler` funciona. Para tanto é preciso
associar um mongomotor.Document a este handler e mapear uma url para fazer uso
disto. Algo assim:

.. code-block::  python

    >>> from pyrocumulus.web.handlers import RestHandler
    >>> from myapp.models import MyDocument
    >>> from tornado.web import URLSpec
    >>> urlspec = URLSpec('/api/mydocument/(.*)', RestHandler, model=MyDocument)

Quando uma url é mapeada para :class:`pyrocumulus.web.handlers.RestHandler`
a api criada tem as seguintes operações:

* Request GET para ``URL_DO_DOCUMENTO`` com uma chave primária para recuperar
  um documento específico.

  .. code-block:: python

      >>> requests.get('/api/mydocument/?id=ALGUM-ID')

* Request GET para ``URL_DO_DOCUMENTO`` para listar os objetos.
  Os parâmetros ``page`` e ``max`` podem ser usados para paginação e
  ``order_by`` pode ser usado para ordenação

  .. code-block:: python

      >>> requests.get('/api/mydocument/?page=1&max=10&order_by=-field')

* Request POST para ``URL_DO_DOCUMENTO`` para inserir um objeto
  na base de dados.

  .. code-block:: python

      >>> params = {'name': 'Some name', 'location': 'Somewhere'}
      >>> request.post('/api/mydocument/', params=params)

* Request PUT para ``URL_DO_DOCUMENTO`` para atualizar um objeto
  na base de dados. É preciso usar uma chave primária no request

  .. code-block:: python

      >>> params = {'name': 'Some name', 'location': 'Somewhere',
		    'id': 'some-id'}
      >>> request.put('/api/mydocument/', params=params)

* Request DELETE para ``URL_DO_DOCUMENTO``para apagar um documento
  da base de dados.

  .. code-block:: python

      >>> params = {'id': 'a9982lsdfk34kd'}
      >>> requests.delete('/api/mydocument/', params=params)

Se o documento tem embedded documents eles também precisarão ser mapeados, mas
usando a classe :class:`pyrocumulus.web.handlers.EmbeddedDocumentHandler`.

.. code-block::  python

    >>> from pyrocumulus.web.handlers import EmbbeddeDocumentRestHandler
    >>> from myapp.models import MyDocument, MyEmbeddedDocument
    >>> from tornado.web import URLSpec
    >>> urlspec = URLSpec('/api/mydocument/embeddedfield/(.*)',
                           EmbeddedDocumentRestHandler,
                           model=MyEmbeddedDocument, parent_doc=MyDocument)

Para facilitar isso, o pyrocumulus tem a classe
:class:`pyrocumulus.web.urlmappers.DocumentURLMapper`, que cria todas as
URLSpecs necessárias, incluindo para embedded documents.


DocumentURLMapper
+++++++++++++++++

A classe :class:`pyrocumulus.web.urlmappers.DocumentURLMapper` recebe
um documento e um request handler como parâmetros no construtor, então
todas as urls necessárias para o funcionamento da API.

.. code-block:: python

    >>> from pyrocumulus.web.urlmappers import DocumentURLMapper
    >>> from pyrocumulus.web.handlers import RestHandler
    >>> from myapp.models import MyDocument
    >>> mapper = DocumentURLMapper(MyDocument, RestHandler)
    >>> print(mapper.urls)

Pode-se usar os parâmetros opcionais ``url_prefix`` e ``url_name_prefix``
para controlar como as urls e os nomes das urls são criados.

Uma vez que as urls estiverem prontas, é preciso criar uma instância de
``tornado.web.Application`` para fazer uso destas urls, algo assim:

.. code-block:: python

    >>> from tornado.web import Application
    >>> app = Application(urls)

RestApplication
+++++++++++++++

No tutorial do pyrocumulus nós usamos a classe
:class:`pyrocumulus.web.application.RestApplication` para criar nossa
aplicação. Ela é uma subclasse de :class:`tornado.web.Application` que
recebe uma lista de documentos como parâmetro e cria os mapeamentos de url
para eles. :class:`pyrocumulus.web.application.RestApplication` aceita os
seguintes parâmetros opcionais:

* ``auth`` - Booleano indicando se a API gerada será autenticada.
* ``readonly`` - Booleano indicando se a API gerada será somente-leitura.
* ``prefix`` - Prefixo que será usando como ``url_prefix`` e
  ``url_name_prefix`` no DocumentURLMapper.


.. code-block:: python

    >>> from pyrocumulus.web.applications import RestApplication
    >>> app = RestApplication(MyDocument, auth=True, prefix='/myapp')

Assim é como a API restful é criada automaticamente. Mas uma coisa ainda
precisa ser explicada, como o servidor do tornado serve nossas aplicações.
Isto é feito pelo comando ``runtornado``.

O comando runtornado
++++++++++++++++++++

O comando ``runtornado`` é usando na linha de comando assim:

.. code-block:: sh

    $ python pyromanager.py runtornado

O comportamento padrão do comando é servir as aplicações
listadas na variável de configuração APPLICATIONS, mas pode-se
especificar uma aplicação usando o parâmetro
``--application=myapp.web.someapp``.

A porta em que o servidor escuta por padrão é definida na variável
de configuração TORNADO_PORT, mas pode ser alterada na linha de comando
com o parâmetro ``--port=1234``.

Para executar o servidor como um daemon, use o parâmetro ``--daemonize``

É isso.


.. [#] Para um melhor entendimento de como RequestHandler funciona, você
       deveria ler a documentação do tornado `aqui
       <http://www.tornadoweb.org/en/stable/web.html#request-handlers>`_
