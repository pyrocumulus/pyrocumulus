.. Pyrocumulus documentation master file, created by
   sphinx-quickstart on Tue Nov 26 19:17:54 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


|pyrocumulus-logo|

.. |pyrocumulus-logo| image:: ./_static/pyrocumulus.png
    :width: 600px
    :alt: APIs restful fáceis para documentos mongodb


APIs restful rápidas e fáceis com mongodb e tornado!

Escreva alguns modelos usando
`MongoMotor <http://mongomotor.poraodojuca.net/ptbr/>`_, crie uma nova
instância de :class:`pyrocumulus.web.applications.RestApplication` e é isso,
Você tem pronta uma api restful para seus dados no mongodb.

Features
++++++++

* API restful automática com suporte opcional a CORS. A api pode ser aberta,
  somente-leitura ou com autenticação.
* Pequenas melhorias no tratamento de arquivos estáticos e templates.


Começando
+++++++++
.. toctree::
   :maxdepth: 1

   intro/installation
   intro/usage
   intro/staticfiles
   intro/templates


Guias
+++++
.. toctree::
   :maxdepth: 1

   guides/restapi


Desenvolvimento
+++++++++++++++
.. toctree::
   :maxdepth: 1

   CHANGELOG


Reference
+++++++++
.. toctree::
   :maxdepth: 1

   apidoc/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
