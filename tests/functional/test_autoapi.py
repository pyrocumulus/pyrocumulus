# messy functional tests. Refactor needed.
# functional tests for the auto api feature.
# here some models are created, an auto restful api is
# configured an then we run tests on it

import asyncio
import json
import requests
import time
from tornado import gen, ioloop
from mongomotor import Document, EmbeddedDocument
from mongomotor.fields import (StringField, ReferenceField,
                               EmbeddedDocumentField, ListField)
from pyrocumulus.auth import AccessToken, Permission
from pyrocumulus.conf import settings
from pyrocumulus.db import MongoConnection
from pyrocumulus.testing import TornadoApplicationTestCase
from pyrocumulus.utils import fqualname
from pyrocumulus.web.applications import RestApplication
from tests import PYVERSION

# mongoengine tutorial

connection = MongoConnection('db-{}'.format(PYVERSION))
connection.connect()


class User(Document):
    email = StringField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)


class Comment(EmbeddedDocument):
    content = StringField()
    name = StringField(max_length=120)


class Post(Document):
    title = StringField(max_length=120, required=True)
    # reverse_delete_rule=CASCADE
    authors = ListField(ReferenceField(User))
    tags = ListField(StringField(max_length=30))
    comments = ListField(EmbeddedDocumentField(Comment))

    meta = {'allow_inheritance': True}


class TextPost(Post):
    content = StringField()


class ImagePost(Post):
    image_path = StringField()


class LinkPost(Post):
    link_url = StringField()


User.ensure_indexes()
Post.ensure_indexes()

application = RestApplication(User, TextPost, LinkPost, ImagePost,
                              url_prefix='/api')
auth_app = RestApplication(User, url_prefix='/userapi', auth=True)


class BaseTestCase(TornadoApplicationTestCase):

    @classmethod
    def setUpClass(cls):
        connection.connect()
        super(BaseTestCase, cls).setUpClass()
        time.sleep(0.1)


class UserAPITestCase(BaseTestCase):

    def setUp(self):
        super(UserAPITestCase, self).setUp()

        self.base_url = 'http://localhost:%s/userapi/user/' % (
            settings.TORNADO_PORT)
        self.id2delete = []
        self.users = []
        self.populate_db()

    @classmethod
    def setUpClass(cls):
        cls.application = 'tests.functional.test_autoapi.auth_app'
        super().setUpClass()
        cls.token = AccessToken()
        cls.raw_token = ioloop.IOLoop.instance().run_sync(cls.token.save)

        @asyncio.coroutine
        def create_perms():
            yield from Permission.create_perms_to(
                cls.token, fqualname(User), 'crud')

        ioloop.IOLoop.instance().run_sync(create_perms)

    @classmethod
    def tearDownClass(cls):
        ioloop.IOLoop.instance().run_sync(cls.token.delete)
        ioloop.IOLoop.instance().run_sync(Permission.objects.all().delete)
        super().tearDownClass()

    def populate_db(self):
        for i in range(3):
            first_name = 'user%s' % i
            last_name = 'da silva'
            email = '%s@email.com' % first_name
            params = {'first_name': first_name, 'last_name': last_name,
                      'email': email}
            headers = {'Authorization': 'token: {}'.format(self.raw_token)}
            response = requests.post(self.base_url, data=json.dumps(params),
                                     headers=headers)
            response.connection.close()

            user = response.json()
            self.users.append(user)

        self.id2delete = [u['id'] for u in self.users]

    def tearDown(self):
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        for i in self.id2delete:
            response = requests.delete(self.base_url, headers=headers,
                                       params={'id': i})
            response.connection.close()

    def test_options_request(self):
        r = requests.options(self.base_url)
        self.assertEqual(r.status_code, 200)

    def test_create(self):
        body = {'email': 'zeninguem@acre.nada', 'first_name': 'ze',
                'last_name': 'ninguem'}
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        response = requests.post(self.base_url, data=json.dumps(body),
                                 headers=headers)

        response.connection.close()
        user = response.json()
        self.assertTrue(user['id'])

        self.id2delete.append(user['id'])

    def test_create_without_required_field(self):
        params = {'first_name': 'ze', 'last_name': 'ninguem'}
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        response = requests.post(self.base_url, params, headers=headers)
        response.connection.close()
        self.assertEqual(response.status_code, 500)

    def test_get(self):
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        response = requests.get(self.base_url,
                                params={'id': self.users[0]['id']},
                                headers=headers)
        response.connection.close()
        self.assertTrue(response.json()['email'])

    def test_list(self):
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        response = requests.get(self.base_url, headers=headers)
        response.connection.close()
        self.assertEqual(len(response.json()['items']), 3)

    def test_list_with_pagination(self):
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        response = requests.get(self.base_url,
                                params={'max': 2, 'page': 1},
                                headers=headers)
        response.connection.close()
        self.assertEqual(len(response.json()['items']), 2)

    def test_list_with_filter(self):
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        response = requests.get(self.base_url, headers=headers,
                                params={'first_name': 'user2'})
        response.connection.close()
        self.assertEqual(len(response.json()['items']), 1)

    def test_delete(self):
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        response = requests.delete(self.base_url,
                                   params={'email': 'user1@email.com'},
                                   headers=headers)
        response.connection.close()
        response = requests.get(self.base_url, headers=headers)
        response.connection.close()
        self.assertEqual(len(response.json()['items']), 2)

    def test_options(self):
        headers = {'Authorization': 'token: {}'.format(self.raw_token)}
        response = requests.options(self.base_url, headers=headers)
        response.connection.close()
        self.assertTrue(response.json()['corsEnabled'])

# by now, our basic functions are tested, let's try something
# better


class BloggingTestCase(BaseTestCase):

    def setUp(self):
        super(BloggingTestCase, self).setUp()
        self.base_url = 'http://localhost:%s/api/textpost/' % (
            settings.TORNADO_PORT)
        self.comments_url = self.base_url + 'comments/'
        self.user_url = 'http://localhost:%s/api/user/' % (
            settings.TORNADO_PORT)
        self.populate_db()
        self.id2delete = []

    @classmethod
    def setUpClass(cls):
        cls.application = 'tests.functional.test_autoapi.application'
        super().setUpClass()
        cls.token = AccessToken()
        ioloop.IOLoop.instance().run_sync(cls.token.save)

        @gen.coroutine
        def create_perms():
            Permission.create_perms_to(cls.token, fqualname(User), 'crud')
            Permission.create_perms_to(cls.token, fqualname(Comment), 'crud')
            Permission.create_perms_to(cls.token, fqualname(Post), 'crud')
            Permission.create_perms_to(cls.token, fqualname(TextPost), 'crud')
            Permission.create_perms_to(cls.token, fqualname(ImagePost), 'crud')
            Permission.create_perms_to(cls.token, fqualname(LinkPost), 'crud')

    @classmethod
    def tearDownClass(cls):
        ioloop.IOLoop.instance().run_sync(cls.token.delete)
        ioloop.IOLoop.instance().run_sync(Permission.objects.all().delete)
        ioloop.IOLoop.instance().run_sync(User.drop_collection)
        ioloop.IOLoop.instance().run_sync(Post.drop_collection)
        super().tearDownClass()

    def populate_db(self):
        self.users = []
        self.posts = []
        for i in range(2):
            u = {'email': 'user%s@email.com' % i}
            response = requests.post(self.user_url, data=json.dumps(u))
            response.connection.close()
            self.users.append(response.json())

            p = {'title': 'post %s' % i, 'authors': [self.users[0]['id']],
                 'content': 'Mussum ipsum cacilds, vidis litro abertis.'}
            response = requests.post(self.base_url, data=json.dumps(p))
            response.connection.close()
            self.posts.append(response.json())

        post = self.posts[1]

        for i in range(10):
            query = {'parent_id': post['id']}
            c = {'name': 'ze da silva %s' % i,
                 'content': 'Mussum ipsum cacilds, vidis litro abertis.'}
            response = requests.post(self.comments_url, data=json.dumps(c),
                                     params=query)
            response.connection.close()

    def tearDown(self):
        self.drop_db()

    def drop_db(self):
        for u in self.users:
            i = u['id']
            response = requests.delete(self.user_url, params={'id': i})
            response.connection.close()

        for p in self.posts:
            i = p['id']
            response = requests.delete(self.base_url, params={'id': i})
            response.connection.close()

        for i in self.id2delete:
            response = requests.delete(self.base_url, params={'id': i})
            response.connection.close()

    def test_show_documentation(self):
        response = requests.get(self.base_url + 'doc')
        response.connection.close()
        self.assertTrue(response.status_code, 200)

    def test_create_post(self):
        params = {'title': 'post test', 'authors': [self.users[0]['id']],
                  'tags': ['bla', 'ble', 'bli'],
                  'content': 'Mussum ipsum cacilds, vidis litro abertis.'}
        response = requests.post(self.base_url, data=json.dumps(params))
        response.connection.close()
        returned_tags = response.json()['tags']
        self.assertEqual(returned_tags, ['bla', 'ble', 'bli'])

        self.id2delete.append(response.json()['id'])

    def test_update_post(self):
        ioloop.IOLoop.instance().run_sync(Post.drop_collection)
        params = {'title': 'post test', 'authors': [self.users[0]['id']],
                  'tags': ['bla', 'ble', 'bli'],
                  'content': 'Mussum ipsum cacilds, vidis litro abertis.'}
        response = requests.post(self.base_url, data=json.dumps(params))
        response.connection.close()
        id = response.json()['id']
        params = {'id': id}
        body = json.dumps({'tags': ['bla', 'new']})
        response = requests.put(self.base_url, params=params, data=body)
        response.connection.close()
        response = requests.get(self.base_url + '?id={}'.format(id))
        response.connection.close()
        self.id2delete.append(response.json()['id'])
        self.assertIn('new', response.json()['tags'])

    def test_list_post_by_tag(self):
        params = {'title': 'post test', 'authors': [self.users[0]['id']],
                  'tags': ['bla', 'ble', 'bli'],
                  'content': 'Mussum ipsum cacilds, vidis litro abertis.'}
        response = requests.post(self.base_url, data=json.dumps(params))
        response.connection.close()
        self.id2delete.append(response.json()['id'])

        params = {'tags': 'bla'}
        response = requests.get(self.base_url, params=params)
        response.connection.close()

        self.assertEqual(len(response.json()['items']), 1)

    def test_list_post_ordering_by_title(self):

        # creating a post to be the last
        params = {'title': 'zx post test the last',
                  'authors': [self.users[0]['id']],
                  'tags': ['last'],
                  'content': 'Mussum ipsum cacilds, vidis litro abertis.'}

        response = requests.post(self.base_url, data=json.dumps(params))
        response.connection.close()
        # now, listing it in reverse order by title
        params = {'order_by': '-title'}
        response = requests.get(self.base_url, params=params)
        response.connection.close()
        first_post = response.json()['items'][0]['title']

        self.assertEqual(first_post, 'zx post test the last')

        self.id2delete.append(response.json()['items'][0]['id'])

    def test_comment_on_post(self):

        parent_id = self.posts[0]['id']
        name = 'ze da silva'
        comment = 'Mussum ipsum cacilds, vidis litro abertis.'
        query = {'parent_id': parent_id}
        body = json.dumps({'name': name, 'content': comment})
        response = requests.post(self.comments_url, params=query, data=body)
        response.connection.close()
        response = requests.get(self.base_url, params={'id': parent_id})
        response.connection.close()

        self.assertEqual(len(response.json()['comments']), 1)
        self.id2delete.append(response.json()['id'])

    def test_list_post_comments(self):

        parent_id = self.posts[1]['id']
        response = requests.get(self.comments_url,
                                params={'parent_id': parent_id})
        response.connection.close()
        self.assertEqual(response.json()['total_items'], 10)

    def test_list_post_comments_with_pagination(self):
        # pagination here is useless for the database, since all
        # EmbeddedDocument's always came in the queries' result, but it is
        # useful for paginating its display
        parent_id = self.posts[1]['id']
        response = requests.get(self.comments_url,
                                params={'parent_id': parent_id,
                                        'page': 2, 'max': 6})
        response.connection.close()

        self.assertEqual(len(response.json()['items']), 4)
