# -*- coding: utf-8 -*-

# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from mock import patch, Mock
from pyrocumulus import db
from pyrocumulus.db import MongoConnection
from tests import PYVERSION


class MongoConnectionTestCase(unittest.TestCase):

    @patch.object(db, 'get_settings_module', Mock())
    def setUp(self):
        db.get_settings_module.return_value.DATABASE = {'default': {}}
        name = 'something-{}'.format(PYVERSION)
        host = 'http://localhost'
        port = '1234'
        self.mongo_connection = MongoConnection(name, host=host, port=port)

    @patch.object(db, 'get_settings_module', Mock())
    def test_init_with_conf_varibles(self):
        db.get_settings_module.return_value.DATABASE = {'default': {
            'NAME': 'test',
            'HOST': 'http://local',
            'PORT': 12345}}
        mongo_connection = MongoConnection('pyrodb-{}'.format(PYVERSION))

        self.assertEqual(mongo_connection.db, 'pyrodb-{}'.format(PYVERSION))

    @patch('pyrocumulus.db.connect')
    def test_connect(self, *args, **kwags):
        self.mongo_connection.connect()
        called_kw = db.connect.call_args[1]
        self.assertEqual(called_kw['async_framework'], 'asyncio')
        self.assertTrue(db.connect.called)

    @patch('pyrocumulus.db.disconnect')
    def test_disconnect(self, *args, **kwargs):
        self.mongo_connection.disconnect()
        self.assertTrue(db.disconnect.called)
