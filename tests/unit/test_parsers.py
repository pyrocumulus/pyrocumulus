# -*- coding: utf-8 -*-

# Copyright 2013-2016 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from tornado.testing import AsyncTestCase
from mock import Mock
from mongomotor import Document, EmbeddedDocument
from mongomotor.fields import (ReferenceField, ListField, BooleanField,
                               EmbeddedDocumentField, StringField, IntField)
from pyrocumulus.parsers import DocumentParser, get_parser


class DocumentParserTestCase(AsyncTestCase):

    def setUp(self):
        super(DocumentParserTestCase, self).setUp()

        class Ref(Document):
            objects = Mock()

        class Embed(EmbeddedDocument):
            objects = Mock()

        class Doc(Document):
            objects = Mock()
            ref = ReferenceField(Ref)
            embed = EmbeddedDocumentField(Embed)
            reflist = ListField(ReferenceField(Ref))
            somethinglist = ListField(StringField())
            embedlist = ListField(EmbeddedDocumentField(Embed))
            stringfield = StringField()
            intfield = IntField()
            SOMETHING = 'bla'
            boolfield = BooleanField()

        self.parser = DocumentParser(Doc)

    def test_get_model_fields(self):
        returned = []
        for name, field in self.parser._get_model_fields():
            returned.append(name)

        expected = ['boolfield', 'embed', 'embedlist', 'id', 'intfield',
                    'ref', 'reflist', 'somethinglist', 'stringfield']

        self.assertEqual(returned, expected)

    def test_fields(self):
        expected = ['id', 'ref', 'embed', 'reflist', 'somethinglist',
                    'embedlist', 'stringfield', 'intfield', 'boolfield']

        returned = [f['name'] for f in self.parser.fields]
        self.assertEqual(expected, returned)

    def test_fields_references(self):
        ref = self.parser.get('ref')
        self.assertTrue(ref['reference_class'])

    def test_references(self):
        expected = ['ref', 'reflist']
        returned = [f['name'] for f in self.parser.references]

        self.assertEqual(expected, returned)

    def test_bool_field(self):
        f = self.parser.get('boolfield')
        self.assertTrue(f['is_bool'])

    def test_lists(self):
        expected = ['reflist', 'somethinglist', 'embedlist']
        returned = [f['name'] for f in self.parser.lists]
        self.assertEqual(expected, returned)

    def test_embeddeds(self):
        expected = ['embed', 'embedlist']
        returned = [f['name'] for f in self.parser.embeddeds]
        self.assertEqual(expected, returned)

    def test_fields_embeddeds(self):
        f = self.parser.get('embedlist')
        self.assertTrue(f['embedded_class'])

    def test_get(self):
        info = self.parser.get('ref')
        self.assertEqual(info['is_reference'], True)


class GetParserTestCase(unittest.TestCase):

    def test_get_parser(self):
        returned = get_parser(Mock())

        self.assertTrue(returned.__class__, DocumentParser)
