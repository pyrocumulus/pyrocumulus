# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from mock import Mock, patch
import pyrocumulus
from pyrocumulus import conf
from pyrocumulus.conf import get_settings_module_name, get_settings_module


class GetSettingsModuleNameTestCase(unittest.TestCase):
    @patch.object(conf.os, 'environ', {})
    def test_get_settings_module_name(self):
        expected_name = 'settings'
        module_name = get_settings_module_name()

        self.assertEqual(expected_name, module_name)

    @patch.object(pyrocumulus.conf.os.environ, 'get',
                  Mock(return_value='some.custom.settings'))
    def test_get_settings_module_name_with_custom_name(self):
        expected_name = 'some.custom.settings'
        module_name = get_settings_module_name()

        self.assertEqual(expected_name, module_name)


@patch.object(pyrocumulus.conf.importlib, 'import_module', Mock())
class GetSettingsModuleTestCase(unittest.TestCase):
    def test_get_settings_module(self):
        get_settings_module()

        self.assertTrue(pyrocumulus.conf.importlib.import_module.called)

    def test_get_settings_module_with_custom_name(self):
        custom_name = 'some.custom.settings'
        get_settings_module(custom_name)

        called_name = pyrocumulus.conf.importlib.import_module.call_args[0][0]

        self.assertEqual(custom_name, called_name)

    @patch.object(pyrocumulus.conf.importlib, 'import_module',
                  Mock(side_effect=ImportError))
    def test_get_settings_module_with_non_existent_module(self):
        pyrocumulus.conf.importlib.import_module.side_effect = ImportError
        with self.assertRaises(ImportError):
            get_settings_module()


@patch.object(conf, 'get_settings_module', Mock())
class SettingsTestCase(unittest.TestCase):
    def setUp(self):
        self.settings = conf.Settings()

    def test_get_settings_attribute(self):
        conf.get_settings_module.return_value.OTHER = True
        self.assertTrue(self.settings.OTHER is True)

    def test_set_settings_attribute_with_error(self):
        with self.assertRaises(AttributeError):
            self.settings.SOME = 1
