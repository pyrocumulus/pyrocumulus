#-*- coding: utf-8 -*-

# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from mock import Mock, patch
from pyrocumulus import testing
from pyrocumulus.testing import TornadoApplicationTestCase


# shit name
@patch.object(testing.os, 'system', Mock())
class TornadoApplicationTestCaseTestCase(unittest.TestCase):
    def test_mount_runtorando_cmd_line(self):
        options = '--something'
        expected = 'python pyromanager.py runtornado --something'
        returned = TornadoApplicationTestCase._mount_runtorando_cmd_line(
            options)

        self.assertEqual(expected, returned)

    @patch.object(TornadoApplicationTestCase, 'application',
                  'some.app_instance')
    def test_setUpClass(self):
        # tests if the correct command
        TornadoApplicationTestCase.setUpClass()
        exp = 'python pyromanager.py runtornado'
        exp += ' --daemonize --application some.app_instance'
        called = testing.os.system.call_args[0][0]

        self.assertEqual(exp, called)

    def test_setUpClass_without_application(self):
        # Tests if the default application is called correctly
        TornadoApplicationTestCase.setUpClass()
        exp = 'python pyromanager.py runtornado --daemonize'
        called = testing.os.system.call_args[0][0]

        self.assertEqual(exp, called)

    @patch.object(TornadoApplicationTestCase, 'application',
                  'some.app_instance')
    def test_tearDownClass(self):
        TornadoApplicationTestCase.tearDownClass()
        exp = 'python pyromanager.py runtornado'
        exp += ' --kill --application some.app_instance'
        called = testing.os.system.call_args[0][0]

        self.assertEqual(exp, called)
