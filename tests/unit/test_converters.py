# -*- coding: utf-8 -*-

# Copyright 2013-2015, 2017 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from unittest import TestCase
import json
import tornado
from bson.objectid import ObjectId
from datetime import datetime
from mock import Mock, patch
from pyrocumulus import converters
from pyrocumulus.converters import get_converter, get_request_converter
from pyrocumulus.converters import (BaseConverter, DocumentConverter,
                                    QuerySetConverter, RequestConverter)
from pyrocumulus.db import MongoConnection
from mongomotor import Document, EmbeddedDocument
from mongomotor.fields import (StringField, DateTimeField, IntField,
                               ReferenceField, EmbeddedDocumentField,
                               ListField, BooleanField)
from tests import PYVERSION, async_test


class BaseConverterTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection('pyrodb-{}'.format(PYVERSION))
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        self.converter = BaseConverter(Mock())

    @patch.object(converters, 'warnings', Mock())
    def test_sanitize_dict(self):
        now = datetime.now()
        oid = ObjectId()
        dict_to_sanitize = {'key1': 'nice value',
                            'key2': 2,
                            'key3': now,
                            'key4': Mock(),
                            'key5': oid}

        expected_dict = {'key1': 'nice value',
                         'key2': 2,
                         'key3': now.strftime('%Y-%m-%d %H:%M:%S'),
                         'key5': str(oid)}

        sanitized = self.converter.sanitize_dict(dict_to_sanitize)

        self.assertEqual(sanitized, expected_dict)

    def test_sanitize_dict_with_nested_dict(self):
        oid = ObjectId()
        dict_to_sanitize = {'key1': 'nice value',
                            'key2': {'other_dict': oid}}

        expected_dict = {'key1': 'nice value',
                         'key2': {'other_dict': str(oid)}}

        sanitized = self.converter.sanitize_dict(dict_to_sanitize)

        self.assertEqual(sanitized, expected_dict)

    def test_sanitize_dict_with_nested_list(self):
        oid = ObjectId()
        dict_to_sanitize = {'key1': 'nice value',
                            'key2': [{'other_dict': oid}]}

        expected_dict = {'key1': 'nice value',
                         'key2': [{'other_dict': str(oid)}]}

        sanitized = self.converter.sanitize_dict(dict_to_sanitize)

        self.assertEqual(sanitized, expected_dict)


class DocumentConverterTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection('pyrodb-{}'.format(PYVERSION))
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super(DocumentConverterTestCase, self).setUp()

        class TestEmbed(EmbeddedDocument):
            hello = StringField()

        class TestRef(Document):
            bla = StringField()
            embed = EmbeddedDocumentField(TestEmbed)

        class TestDoc(Document):
            nice_attr = StringField()
            other_attr = IntField()
            ref = ReferenceField(TestRef)
            date = DateTimeField()
            listfield = ListField(ReferenceField(TestRef))
            stringlist = ListField(StringField())

        embed = TestEmbed(hello='world')
        doc = TestDoc()
        now = datetime.now()
        doc.nice_attr = 'wow!'
        doc.other_attr = 666
        doc.ref = TestRef(bla='asdf', embed=embed)
        doc.date = now
        doc.listfield = [TestRef(bla='oiuy')]
        doc.stringlist = ['asdf', '123']
        self.doc = doc
        self.doc_class = TestDoc
        self.ref_class = TestRef
        # default max_depth is 0
        self.converter = DocumentConverter(doc)

    @async_test
    def tearDown(self):
        yield from type(self.doc).drop_collection()
        yield from type((yield from self.doc.ref)).drop_collection()

    @async_test
    def test_to_dict(self):
        yield from (yield from self.doc.listfield)[0].save()
        yield from (yield from self.doc.ref).save()
        yield from self.doc.save()

        now = self.converter.obj.date
        expected_dict = {'nice_attr': 'wow!',
                         'other_attr': 666,
                         'date': now,
                         'id': self.doc.id,
                         'pk': self.doc.pk}

        return_dict = yield from self.converter.to_dict()
        self.assertEqual(expected_dict, return_dict)

    @async_test
    def test_to_dict_with_depth(self):

        yield from (yield from self.doc.listfield)[0].save()
        yield from (yield from self.doc.ref).save()
        yield from self.doc.save()

        obj = yield from self.doc_class.objects.get(id=self.doc.id)
        converter = DocumentConverter(obj, max_depth=2)

        now = converter.obj.date
        expected_dict = {'nice_attr': 'wow!',
                         'other_attr': 666,
                         'date': now,

                         'id': self.doc.id,
                         'pk': self.doc.pk,
                         'ref': {'id': (yield from self.doc.ref).id,
                                 'pk': (yield from self.doc.ref).pk,
                                 'bla': 'asdf',
                                 'embed': {'hello': 'world'},
                                 },
                         'listfield': [
                             {'id': (yield from self.doc.listfield)[0].id,
                              'pk': (yield from self.doc.listfield)[0].pk,
                              'bla': 'oiuy',
                              'embed': {}}
                                       ],
                         'stringlist': ['asdf', '123']}

        return_dict = yield from converter.to_dict()
        self.assertEqual(expected_dict, return_dict)

    @async_test
    def test_to_json(self):
        now = self.converter.obj.date

        expected = {'date': now.strftime('%Y-%m-%d %H:%M:%S'),
                    'pk': None,
                    'nice_attr': 'wow!',
                    'id': None,
                    'other_attr': 666}

        returned_json = yield from self.converter.to_json()
        returned = json.loads(returned_json)

        # compare dicts not json because dicts have no
        # order on keys so it can be wrong comparing json
        # (str like) made from dicts
        self.assertEqual(returned, expected)


class QuerySetConverterTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection('pyrodb-{}'.format(PYVERSION))
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super(QuerySetConverterTestCase, self).setUp()

        class TestDoc(Document):
            a = StringField()
            b = IntField()

        self.doc = TestDoc

    @async_test
    def test_to_dict(self):
        obj = self.doc(a='asdf', b=1)
        yield from obj.save()
        self.converter = QuerySetConverter(self.doc.objects)
        expected_result = {'items': [{'a': 'asdf', 'b': 1,
                                      'id': obj.id, 'pk': obj.pk}],
                           'quantity_items': 1}

        returned = yield from self.converter.to_dict()

        self.assertEqual(expected_result, returned)

        yield from self.doc.drop_collection()

    @async_test
    def test_to_json(self):
        obj = self.doc(a='asdf', b=1)
        yield from obj.save()
        self.converter = QuerySetConverter(self.doc.objects)
        expected = {'items': [{'a': 'asdf', 'b': 1,
                               'id': str(obj.id), 'pk': str(obj.pk)}],
                    'quantity_items': 1}

        returned_json = yield from self.converter.to_json()
        returned = json.loads(returned_json)

        yield from self.doc.drop_collection()

        self.assertEqual(returned, expected)


class GetConverterTestCase(unittest.TestCase):

    def test_get_converter_with_document_subclass(self):

        class TestDoc(Document):
            a = StringField()
            b = IntField()

        test_doc = TestDoc(a='asdf', b=1)

        converter = get_converter(test_doc)

        self.assertTrue(isinstance(converter, DocumentConverter))

    def test_get_converter_with_queryset(self):

        class TestDoc(Document):
            a = StringField()
            b = IntField()

        test_doc = TestDoc(a='asdf', b=1)
        my_queryset = [test_doc]

        converter = get_converter(my_queryset)

        self.assertTrue(isinstance(converter, QuerySetConverter))


class GetRequestConverterTestCase(unittest.TestCase):

    def test_get_request_converter(self):
        returned = get_request_converter({}, '{}'.encode('utf-8'), {})

        self.assertEqual(returned.__class__, RequestConverter)


class RequestConverterTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection('pyrodb-{}'.format(PYVERSION))
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super(RequestConverterTestCase, self).setUp()

        class Ref(Document):
            a = StringField()

        class Model(Document):
            reflist = ListField(ReferenceField(Ref))
            ref = ReferenceField(Ref)
            objlist = ListField(StringField())
            date = DateTimeField()
            intf = IntField()
            boolf = BooleanField()
            other_bool = BooleanField()

        now = datetime.now()
        self.now = datetime(now.year, now.month, now.day,
                            now.hour, now.minute, now.second)
        self.ref_class = Ref
        self.model = Model
        self.request_arguments = {}
        self.request_body_dict = {
            'objlist': ['one-tag', 'other-tag'],
            'date': self.now.strftime('%Y-%m-%d %H:%M:%S'),
            'intf': 1,
            'boolf': 'false',
            'other_bool': True}

        self.request_body = json.dumps(self.request_body_dict).encode('utf-8')

    def test_to_json(self):
        converter = RequestConverter(self.request_arguments,
                                     self.request_body, self.model)
        with self.assertRaises(NotImplementedError):
            converter.to_json()

    @async_test
    def test_get_body_simple(self):
        # tests the simplest case, without references, bytes or
        # anything.
        converter = RequestConverter(self.request_arguments,
                                     self.request_body, self.model)

        expected = {'objlist': ['one-tag', 'other-tag'], 'date': self.now,
                    'intf': 1, 'boolf': False, 'other_bool': True}
        returned = yield from converter.get_body()

        self.assertEqual(expected, returned)

    @async_test
    def test_get_body_with_reference(self):
        # Tests a request with an reference.
        # The returned object should have an instance
        # of the refered object.
        ref = self.ref_class(a='a')
        yield from ref.save()

        self.request_body_dict.update({'ref__id': str(ref.id)})
        self.request_body = json.dumps(self.request_body_dict).encode('utf-8')
        converter = RequestConverter(self.request_arguments, self.request_body,
                                     self.model)

        expected = {'objlist': ['one-tag', 'other-tag'],
                    'intf': 1,
                    'ref': ref,
                    'date': self.now,
                    'boolf': False,
                    'other_bool': True}

        returned = yield from converter.get_body()

        self.assertEqual(returned, expected)
        self.assertEqual(returned['ref'].a, 'a')

    @async_test
    def test_get_body_with_non_existent_reference(self):
        # tests request with a non existent referece.
        # should return None for the reference
        side_effect = self.ref_class.DoesNotExist
        self.ref_class.objects = Mock()
        self.ref_class.objects.get.side_effect = side_effect

        ref = self.ref_class(id='0123456789')
        self.request_body_dict.update({'ref__id': str(ref.id)})
        self.request_body = json.dumps(self.request_body_dict).encode('utf-8')

        converter = RequestConverter(self.request_arguments, self.request_body,
                                     self.model)
        returned = yield from converter.get_body()
        self.assertEqual(returned['ref'].id, ref.id)
        self.assertIsNone(returned['ref'].a)

    @async_test
    def test_get_body_with_reference_list(self):
        ref = self.ref_class()
        yield from ref.save()

        self.request_body_dict.update({'reflist': [str(ref.id)]})
        self.request_body = json.dumps(self.request_body_dict).encode('utf-8')
        expected = {'objlist': ['one-tag', 'other-tag'],
                    'intf': 1,
                    'reflist': [str(ref.id)],
                    'date': self.now,
                    'boolf': False,
                    'other_bool': True}
        converter = RequestConverter(self.request_arguments, self.request_body,
                                     self.model)

        returned = yield from converter.get_body()

        self.assertEqual(expected, returned)

    def test_is_date(self):
        value = '2013-12-19 00:40:00'
        converter = RequestConverter(self.request_arguments, self.request_body,
                                     self.model)

        self.assertTrue(converter._is_date(value))

    def test_is_date_with_wrong_format(self):
        value = '2013-12-19 00:'
        converter = RequestConverter(self.request_arguments, self.request_body,
                                     self.model)

        self.assertFalse(converter._is_date(value))

    @async_test
    def test_is_listfield(self):
        key = 'reflist'
        converter = RequestConverter(self.request_arguments, self.request_body,
                                     self.model)

        self.assertTrue(converter._is_listfield(key))

    def test_get_bool_value(self):
        converter = RequestConverter(self.request_arguments, self.request_body,
                                     self.model)
        fvalue = converter._get_bool_value('false')
        self.assertFalse(fvalue)
