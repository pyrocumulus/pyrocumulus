# -*- coding: utf-8 -*-

# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from mock import Mock, patch
from tornado.testing import AsyncTestCase
from pyrocumulus.exceptions import (PyrocumulusCommandNotFound,
                                    PyrocumulusException)
from pyrocumulus.commands import base
from pyrocumulus.commands.runtornado import RunTornadoCommand


@patch.object(base.os, 'environ', {})
class BaseCommandTestCase(AsyncTestCase):

    @patch.object(base, 'argparse', Mock())
    @patch.object(base.os, 'environ', {})
    def setUp(self):
        super(BaseCommandTestCase, self).setUp()

        options = Mock()
        options.my_special_param = 'special'
        options.another_param = False
        parser = base.argparse.ArgumentParser.return_value
        parser.parse_args.return_value = options
        self.command = base.BaseCommand()
        self.command.name = 'tester'

    def test_add_args_add_command_name(self):
        # tests if the command name was added correctly
        # to the arguments list
        self.command.parser.add_argument = Mock()
        self.command.add_args()
        called_args = self.command.parser.add_argument.call_args_list[0][0]
        expected_args = ('tester',)

        self.assertEqual(called_args, expected_args)

    def test_add_args(self):
        # tests if the args were added correctly to the
        # parser
        self.command.parser.add_argument = Mock()
        self.command.user_options = [{'args': ('--something',),
                                      'kwargs': {'bla': 'ble',
                                                 'default': None}}]
        self.command.add_args()
        called_args = self.command.parser.add_argument.call_args_list[1][0]
        expected_args = ('--something',)
        called_kwargs = self.command.parser.add_argument.call_args_list[1][1]
        expected_kwargs = {'bla': 'ble', 'default': None}

        self.assertEqual((called_args, called_kwargs),
                         (expected_args, expected_kwargs))

    @patch.object(base, 'argparse', Mock())
    def test_parse_args(self):
        # tests if the options attributes are
        # set correcty in the command object
        options = Mock()
        options.my_special_param = 'special'
        options.another_param = False
        parser = base.argparse.ArgumentParser.return_value
        parser.parse_args.return_value = options
        self.command.parse_args()

        self.assertEqual(self.command.my_special_param, 'special')

    def test_run(self):
        # Method run() from BaseCommand is not implemented

        with self.assertRaises(NotImplementedError):
            self.command.run()

    def test_get_command_name(self):
        self.command.__class__.__module__ = 'some.module.command'

        command_name = self.command.get_command_name()

        self.assertEqual(command_name, 'command')

    def test_call(self):
        self.command.run = Mock()
        self.command()

        self.assertTrue(self.command.run.called)


@patch.object(base, 'importlib', Mock())
class GetCommandTestCase(unittest.TestCase):

    def test_get_command_with_valid_command(self):
        # tests if the returned value is the correct one
        module_mock = Mock()
        module_mock.a_attr = RunTornadoCommand
        base.importlib.import_module.return_value = module_mock
        command = base.get_command('runtornado')

        self.assertEqual(command, RunTornadoCommand)

    def test_get_command_with_invalid_command(self):
        # tests if PyrocumulusCommandNotFound is raised
        # when a command is not found
        with self.assertRaises(PyrocumulusCommandNotFound):

            base.get_command('bla')

    @patch.object(base, 'importlib', Mock())
    @patch('pyrocumulus.conf.settings')
    def test_get_command_with_more_packages(self, *args, **kwargs):
        # tests if a command on a custom module (included via settings)
        # is found by get_command()

        class FakeCommand(base.BaseCommand):
            pass

        module_mock = Mock()
        module_mock.a_attr = FakeCommand
        base.importlib.import_module.return_value = module_mock

        import pyrocumulus
        pyrocumulus.conf.settings.COMMAND_PACKAGES = ['some.other.commands']
        command = base.get_command('fake')

        self.assertEqual(command, FakeCommand)

        self.assertEqual(base.importlib.import_module.call_args[0][0],
                         'some.other.commands.fake')

    @patch.object(base, 'importlib', Mock())
    @patch('pyrocumulus.conf.settings')
    def test_get_command_with_custom_command(self, *args, **kwargs):
        # tests if a command on a custom module (included via settings)
        # is found by get_command()
        try:

            class FakeCommand(base.BaseCommand):
                pass

            module_mock = Mock()
            module_mock.a_attr = FakeCommand
            base.importlib.import_module.side_effect = [ImportError,
                                                        module_mock]

            import pyrocumulus
            pyrocumulus.conf.settings.COMMAND_PACKAGES = [
                'some.other.commands',
                'some.package.command']
            command = base.get_command('fake')

            self.assertEqual(command, FakeCommand)

            self.assertEqual(base.importlib.import_module.call_args[0][0],
                             'some.package.command.fake')
        finally:
            base.importlib.import_module.side_effect = None

    @patch.object(base, 'importlib', Mock())
    def test_get_command_with_extended_command(self, *args, **kwargs):
        # tests if a command that extends other command works properly

        try:
            class FakeCommand(base.BaseCommand):
                pass

            class OtherCommand(FakeCommand):
                pass

            module_mock = Mock()
            module_mock.a_attr = FakeCommand
            module_mock.b_attr = OtherCommand
            base.importlib.import_module.return_value = module_mock

            command = base.get_command('other')

            self.assertEqual(command, OtherCommand)
        finally:
            base.importlib.import_module.return_value = Mock()


class GetCommandNameTestCase(unittest.TestCase):

    @patch.object(base.sys, 'argv', ['python', 'pyromanager.py',
                                     'somecommand'])
    def test_get_command_name_called_with_python(self):
        # tests if the command line is getted correctly when the script
        # is called with python exec as first argument of the command
        # line

        cmdname = base.get_command_name()
        self.assertEqual(cmdname, 'somecommand')

    @patch.object(base.sys, 'argv', ['pyromanager.py'])
    def test_get_command_name_with_invalid_command_line(self):
        with self.assertRaises(PyrocumulusException):
            base.get_command_name()

    @patch.object(base.sys, 'argv', ['pyromanager.py', 'othercommand'])
    def test_get_command_name(self):
        # tests if the command line is getted correctly when the script
        # is called without python exec as first argument of the command
        # line

        cmdname = base.get_command_name()
        self.assertEqual(cmdname, 'othercommand')


class GetSettingsModuleNameTestCase(unittest.TestCase):

    @patch.object(base.sys, 'argv', ['pyromanager.py', 'somecommand'])
    def test_get_settings_module_name_without_module_name(self):
        # Tests if the settings module is setted correctly to 'settings' when
        # no settings is used on command line
        settings_name = base.get_settings_module_name()
        self.assertIs(settings_name, None)

    @patch.object(base.sys, 'argv', ['pyromanager.py', 'somecommand',
                                     '--settings=some.settings'])
    def test_get_settings_module_name(self):
        # Tests if the settings module is setted correctly to 'settings' when
        # a different settings is used on command line
        settings_name = base.get_settings_module_name()
        self.assertEqual(settings_name, 'some.settings')


class SetenvTestCase(unittest.TestCase):

    @patch.object(base.os, 'environ', {})
    @patch.object(base.sys, 'argv', ['pyromanager.py', 'somecommand',
                                     '--settings=some.settings'])
    def test_setenv(self):
        # test if PYROCUMULUS_SETTINGS_MODULE env var was setted
        # correctly
        base.setenv()
        called = base.os.environ['PYROCUMULUS_SETTINGS_MODULE']
        expected = 'some.settings'
        self.assertEqual(called, expected)


class RunCommandTestCase(unittest.TestCase):
    # Tests if the run() method is called
    # on the command returned by get_command

    @patch.object(base, 'get_command_name', Mock())
    @patch.object(base, 'get_command', Mock())
    def test_run_command(self):
        command = base.get_command.return_value.return_value
        base.run_command()
        self.assertTrue(command.run.called)

    @patch.object(base, 'get_command_name', Mock())
    @patch.object(base, 'show_help_message', Mock())
    def test_run_command_help(self):
        base.get_command_name.return_value = '--help'
        base.run_command()

        self.assertTrue(base.show_help_message.called)


class ListCommandsTestCase(unittest.TestCase):

    @patch.object(base.os, 'listdir', Mock())
    def test_list_commands(self):
        base.os.listdir.return_value = ['base.py', 'onecommand.py',
                                        'othercommand.py', 'othercommand.py~',
                                        '__pycache__']

        expected = ['onecommand', 'othercommand']
        returned = base.list_commands()

        self.assertEqual(expected, returned)
