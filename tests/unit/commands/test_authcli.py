# -*- coding: utf-8 -*-

# Copyright 2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus. If not, see <http://www.gnu.org/licenses/>.

import unittest
from unittest import mock
import tornado
from pyrocumulus import auth
from pyrocumulus import commands
from pyrocumulus.commands import authcli
from pyrocumulus.db import MongoConnection
from tests import PYVERSION


class AuthCliTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.conn = MongoConnection('pyrodb-{}'.format(PYVERSION))
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()

    def setUp(self):
        self.authcli = authcli.AuthCli()
        self.authcli.print2screen = mock.Mock()

    def tearDown(self):
        auth.AccessToken.drop_collection()
        auth.Permission.drop_collection()

    def get_new_ioloop(self):
        return tornado.ioloop.IOLoop.instance()

    def test_create_token(self):
        uncrypted_token, token = self.authcli.create_token('test-token')

        @tornado.gen.coroutine
        def get_token():
            tk = yield auth.AccessToken.get_by_token(uncrypted_token)
            return tk

        ntoken = self.authcli.ioloop.run_sync(get_token)
        self.assertEqual(token, ntoken)

    def test_add_token(self):
        name = 'test-token'
        perms = [('pyrocumulus.auth.AccessToken', 'rw'),
                 (auth.Permission, 'w')]
        domains = ['somedomain.com', 'somedomain.net']
        uncrypted_token = self.authcli.add_token(name, domains, *perms)

        @tornado.gen.coroutine
        def get_token():
            tk = yield auth.AccessToken.get_by_token(uncrypted_token)
            return tk

        token = self.authcli.ioloop.run_sync(get_token)

        self.assertTrue(token)

    def test_list_tokens(self):
        perms = [('pyrocumulus.auth.AccessToken', 'rw'),
                 (auth.Permission, 'w')]
        self.authcli.add_token('tk1', *perms)
        self.authcli.add_token('tk2', *perms)

        tokens = self.authcli.list_tokens()

        self.assertEqual(len(tokens), 2)

    def test_del_token(self):
        perms = [('pyrocumulus.auth.AccessToken', 'rw'),
                 (auth.Permission, 'w')]
        uncrypted_token = self.authcli.add_token('tk1', *perms)

        self.authcli.del_token(uncrypted_token)

        self.assertEqual(len(self.authcli.list_tokens()), 0)


@mock.patch.object(authcli, 'AuthCli', mock.Mock())
class AuthCliCommandTest(unittest.TestCase):

    @mock.patch.object(commands.base, 'argparse', mock.Mock())
    def setUp(self):
        parser = commands.base.argparse.ArgumentParser.return_value
        parser.parse_args.return_value = []

        self.command = authcli.AuthCliCommand()

        self.command.perms = 'pyrocumulus.auth.AccessToken:rw'
        self.command.perms += ',pyrocumulus.auth.Permission:w'
        self.command.domains = 'bla.com,01.bla.com,ble.com'

        self.command.token_name = 'tk1'
        self.command.token = '2123sdf0980'

    def test_run_with_add_token_action(self):
        self.command.action = 'add_token'

        self.command.run()

        self.assertTrue(self.command.cli.add_token.called)

    def test_run_with_list_tokens_action(self):
        self.command.action = 'list_tokens'

        self.command.run()

        self.assertTrue(self.command.cli.list_tokens.called)

    def test_run_with_del_token_action(self):
        self.command.action = 'del_token'

        self.command.run()

        self.assertTrue(self.command.cli.del_token.called)

    def test_run_with_unknown_action(self):
        self.command.action = 'unknown'

        self.command.run()

        msg = self.command.cli.print2screen.call_args[0][0]
        self.assertEqual(msg, 'Action unknown does not exist!')
