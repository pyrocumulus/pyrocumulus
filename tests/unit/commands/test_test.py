#-*- coding: utf-8 -*-

# tragic file name.

import unittest
from mock import Mock, patch
from tornado.testing import AsyncTestCase, gen_test
from pyrocumulus import commands
from pyrocumulus.commands import test


@patch.object(test, 'Distribution', Mock())
@patch.object(test, 'test', Mock())
class TestCommandTestCase(AsyncTestCase):

    @patch.object(commands.base, 'argparse', Mock())
    def setUp(self):
        super(TestCommandTestCase, self).setUp()

        self.cmd = test.TestCommand()
        self.cmd.test_suite = None

    def test_run_with_default_test_suite(self):
        self.cmd.run()

        self.assertEqual(self.cmd.test_suite, 'tests')

    def test_run_with_custom_test_suite(self):
        self.cmd.test_suite = 'test_suite'
        self.cmd.run()

        self.assertEqual(self.cmd.test_suite, 'test_suite')
