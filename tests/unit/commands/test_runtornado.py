# -*- coding: utf-8 -*-

# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import _io
from unittest import TestCase
from mock import Mock, MagicMock, patch
from pyrocumulus import commands
from pyrocumulus.commands import runtornado
from pyrocumulus.commands.runtornado import RunTornadoCommand
from pyrocumulus.web import applications


@patch.object(runtornado.os, 'fork', Mock())
@patch.object(runtornado.os, 'setsid', Mock())
@patch.object(runtornado.os, 'kill', Mock())
@patch.object(runtornado.os, 'close', Mock())
@patch.object(runtornado.os, 'mkdir', Mock())
@patch.object(runtornado.os, 'makedirs', Mock())
@patch.object(runtornado.os, 'remove', Mock())
@patch.object(runtornado.os, 'dup2', Mock())
@patch.object(runtornado.sys, 'stdin', Mock())
@patch.object(runtornado.sys, 'stdout', Mock())
@patch.object(runtornado.sys, 'stderr', Mock())
@patch.object(runtornado.sys, 'exit', Mock())
@patch.object(runtornado.ioloop, 'IOLoop', Mock())
@patch.object(commands.runtornado, 'import_module', Mock())
@patch.object(commands.runtornado, 'AsyncIOMainLoop', Mock())
@patch.object(commands.runtornado.HTTPServer, 'listen', Mock())
class RunTornadoCommandTestCase(TestCase):

    @patch.object(commands.base, 'argparse', Mock())
    def setUp(self):
        super(RunTornadoCommandTestCase, self).setUp()

        parser = commands.base.argparse.ArgumentParser.return_value
        parser.parse_args.return_value = []
        self.command = RunTornadoCommand()
        self.command.application = 'some.app'
        self.command.daemonize = False
        self.command.kill = False
        self.command.port = None
        self.command.stdout = 'logs/stdout.log'
        self.command.stderr = 'logs/stderr.log'
        self.command.pidfile = 'runtornado.pid'
        self.command.asyncio = False
        self.command.loglevel = 'info'

    def test_do_fork_with_pid_not_zero(self):
        # tests if sys.exit was called in the
        # parent process
        runtornado.sys.exit.called = False
        runtornado.os.fork.return_value = 1
        self.command._do_fork()

        self.assertTrue(runtornado.sys.exit.called)

    def test_do_fork_with_pid_zero(self):
        # tests if sys.exit was not called in the
        # child process
        runtornado.sys.exit.called = False
        runtornado.os.fork.return_value = 0
        self.command._do_fork()

        self.assertFalse(runtornado.sys.exit.called)

    @patch.object(RunTornadoCommand, 'run_as_a_daemon', Mock())
    @patch.object(RunTornadoCommand, '_write_to_file', Mock())
    def test_run_daemonized(self):
        # tests if run_as_a_daemon is called when daemonize = True
        self.command.daemonize = True
        with patch('pyrocumulus.commands.runtornado.open', create=True):
            self.command.run()
        self.assertTrue(self.command.run_as_a_daemon.called)

    @patch.object(RunTornadoCommand, 'run_as_a_daemon', Mock())
    @patch.object(RunTornadoCommand, '_write_to_file', Mock())
    def test_run_not_daemonized(self):
        # tests if run_as_a_daemon is not called when daemonize = False
        self.command.daemonize = False
        self.command.run()

        self.assertFalse(self.command.run_as_a_daemon.called)

    @patch.object(RunTornadoCommand, 'killtornado', Mock())
    def test_run_to_kill(self):
        # tests if killtornado() is called when kill = True
        self.command.kill = True
        self.command.run()

        self.assertTrue(self.command.killtornado.called)

    def test_run_as_daemon(self):
        self.assertFalse(self.command.run_as_a_daemon())

    @patch.object(runtornado.RunTornadoCommand, '_read_file',
                  Mock(side_effect=IOError))
    def test_killtornado_with_no_pid_file(self):
        # tests if return False when killtornado can't find
        # pidfile for tornado
        self.assertFalse(self.command.killtornado())

    @patch.object(runtornado.RunTornadoCommand, '_read_file',
                  Mock(return_value=123123))
    def test_killtornado(self):
        # tests if return True when killtornado runs ok
        self.assertTrue(self.command.killtornado())

    @patch.object(runtornado.RunTornadoCommand, '_read_file', Mock())
    def test_killtornado_with_bad_content_on_pid_file(self):
        # tests if return False when bad content is on
        # tornado pidfile
        self.assertFalse(self.command.killtornado())

    def test_read_file(self):
        # tests if read() method of a file is called
        with patch('pyrocumulus.commands.runtornado.open',
                   create=True) as open_mock:
            open_mock.return_value = MagicMock(spec=_io.TextIOWrapper)
            fhandle = open_mock.return_value.__enter__.return_value
            self.command._read_file('fname')

            self.assertTrue(fhandle.read.called)

    def test_write_to_file(self):
        # tests if write() method of a file is called
        with patch('pyrocumulus.commands.runtornado.open',
                   create=True) as open_mock:
            open_mock.return_value = MagicMock(spec=_io.TextIOWrapper)
            fhandle = open_mock.return_value.__enter__.return_value
            self.command._write_to_file('fname', 11)

            self.assertTrue(fhandle.write.called)

    def test_try_create_required_dirs_and_files(self):
        # tests if the correct dir is created, verifying the
        # parameters passed to os.mkdir()
        with patch('pyrocumulus.commands.runtornado.open', create=True):
            runtornado.os.mkdir.side_effect = [OSError, None, None]
            self.command._try_create_required_dirs_and_files()
            called_dir = runtornado.os.mkdir.call_args_list[1][0][0]
            expected_dir = 'logs'

        self.assertEqual(called_dir, expected_dir)

    def test_close_file_descriptors(self):
        # only tests if sys.stdin.close was called
        self.command.close_file_descriptors()
        self.assertTrue(runtornado.sys.stdin.close.called)

    def test_redirect_file_stdout_stderr(self):
        # tests if os.dup2 was called with the correct file handlers
        with patch('pyrocumulus.commands.runtornado.open',
                   create=True) as magic_mock:
            self.command.redirect_stdout_stderr()
        fileno = magic_mock.return_value.fileno.return_value
        fileno_sys = runtornado.sys.stderr.fileno.return_value
        called_args = runtornado.os.dup2.call_args[0]

        self.assertEqual((fileno, fileno_sys), called_args)

    def test_get_application(self):
        # tests if the application is returned correctly
        self.command.application = 'some.module.app_instance'
        module_mock = Mock()
        app_mock = Mock()
        module_mock.app_instance = app_mock
        runtornado.import_module.return_value = module_mock

        app = self.command.get_application()
        self.assertEqual(app, app_mock)

    @patch.object(applications, 'settings', MagicMock())
    def test_get_application_with_default_application(self):
        # tests if pyrocumulus.web.applications.get_main_application
        # is called.

        applications.settings.TORNADO_OPTS = {}
        self.command.application = None
        main_app = self.command.get_application()

        self.assertIsInstance(main_app.urls, list)

    @patch.object(runtornado.os.path, 'exists', Mock(return_value=False))
    def test_redirect_file_stdout_stderr_create_dirs(self):
        # tests if os.makedirs is called correctly
        with patch('pyrocumulus.commands.runtornado.open', create=True):
            self.command.redirect_stdout_stderr()

        expected = ['logs', 'logs']
        called = [runtornado.os.makedirs.call_args_list[0][0][0]]
        called.append(runtornado.os.makedirs.call_args_list[1][0][0])

        self.assertEqual(expected, called)

    def test_get_port_with_command_line_param(self):
        # tests if the port is set correctly when
        # the command line parameter is used
        self.command.port = 9999
        port = self.command.get_port()

        self.assertEqual(port, 9999)

    @patch.object(runtornado, 'get_value_from_settings',
                  Mock(return_value=7777))
    def test_get_port_with_settings_param(self):
        # tests if the port is set correctly when
        # getting it from settings
        port = self.command.get_port()

        self.assertEqual(port, 7777)

    def test_get_port_with_default_value(self):
        # tests the default value for port
        port = self.command.get_port()

        self.assertEqual(port, 8888)

    def test_run_with_asyncio(self):
        self.command.asyncio = True
        self.command.run()
        self.assertTrue(runtornado.AsyncIOMainLoop.called)

    @patch.object(runtornado.logging, 'basicConfig', Mock())
    def test_set_log_level(self):
        self.command.loglevel = 'debug'
        self.command._set_log_level()
        called = runtornado.logging.basicConfig.call_args[1]
        # logging.DEBUG is 10
        expected = {'level': 10}
        self.assertEqual(called, expected)
