#-*- coding: utf-8 -*-

import unittest
from mock import Mock, patch
from tornado.testing import AsyncTestCase, gen_test
from pyrocumulus import commands
from pyrocumulus.commands import createproject


SETUP_TEMPLATE = """#-*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(name='%(name)s',
      version='0.1',
      author='You Buddy',
      author_email='you@somewhere.com',
      description='%(name)s is really cool software',
      long_description="It's based on pyrocumulus and have nice features!",
      url='https://some-url-to-my-project.com',
      packages=find_packages(),
      install_requires=['pyrocumulus'],
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Programming Language :: Python :: 3',
      ],
      test_suite='tests',
      provides=['%(name)s'],
)
"""


class CreateProjectCommandTestCase(AsyncTestCase):
    @patch.object(createproject.os, 'environ', {})
    @patch.object(commands.base, 'argparse', Mock())
    def setUp(self):
        super(CreateProjectCommandTestCase, self).setUp()

        self.command = createproject.CreateProjectCommand()
        self.command.path = '/some/dir'
        self.command.project_name = 'myproject'

    @patch.object(createproject.os, 'mkdir', Mock(side_effect=OSError))
    def test_run_with_diretory_creation_exception(self):
        # Tests if OSError is raised when can't create dir
        with self.assertRaises(OSError):
            with patch('pyrocumulus.commands.createproject.print',
                       create=True):
                self.command.run()

    @patch.object(createproject.os, 'mkdir', Mock())
    def test_run(self):
        template = SETUP_TEMPLATE % {'name': self.command.project_name}
        with patch('pyrocumulus.commands.createproject.open',
                   create=True) as open_mock:
            with patch('pyrocumulus.commands.createproject.print',
                       create=True):
                self.command.run()
                called = open_mock.return_value.__enter__.return_value.write.\
                         call_args_list[0][0][0]
                self. assertEqual(template, called)
