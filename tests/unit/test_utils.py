# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from mock import Mock, patch
from pyrocumulus import utils
from pyrocumulus.web.handlers import RestHandler


@patch.object(utils, 'settings', Mock())
class UtilsTestCase(unittest.TestCase):
    def test_get_value_from_settings(self):
        # tests if the retuned value is the same as
        # the value in settings
        utils.settings.SOMETHING = 'value'
        returned = utils.get_value_from_settings('SOMETHING')
        expected = 'value'

        self.assertEqual(returned, expected)

    def test_fqualname(self):
        expected = 'pyrocumulus.web.handlers.RestHandler'
        returned = utils.fqualname(RestHandler)

        self.assertEqual(returned, expected)

    def test_bcrypt_string(self):
        r = utils.bcrypt_string('asdf')
        self.assertTrue(r)

    def test_test_bcrypt_string_salt(self):
        salt = utils.bcrypt.gensalt(8)
        r = utils.bcrypt_string('asdf', salt)
        self.assertTrue(r.startswith(salt.decode()))
