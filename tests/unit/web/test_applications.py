# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

from tornado.testing import AsyncTestCase
from mock import Mock, patch
from mongomotor import Document
from mongomotor.fields import StringField
from pyrocumulus.web import applications


class PyroApplicationTestcase(AsyncTestCase):

    @patch('tornado.routing.import_object')
    def test_urls(self, *args, **kwargs):
        app = applications.PyroApplication([('/some/url', 'some.Class')])
        expected = ['/some/url']

        urls = [u.reverse() for u in app.urls]

        self.assertEqual(urls, expected)

    def test_urls_index_error(self, *args, **kwargs):
        app = applications.PyroApplication([])

        self.assertEqual(app.urls, [])


class RestApplicationTestCase(AsyncTestCase):

    def setUp(self):
        super(RestApplicationTestCase, self).setUp()

        class TestDoc(Document):
            bla = StringField()

        self.application = applications.RestApplication(
            TestDoc, url_prefix='/test')

    def test_create_urls(self):
        # urls without the extra_urls
        expected = ['/test/testdoc/(.*)$']
        self.application.create_urls()
        urls_created = [u.regex.pattern for u in self.application.urls]
        self.assertEqual(expected, urls_created)


class StaticApplicationTestCase(AsyncTestCase):

    @patch.object(applications, 'settings', Mock())
    def test_static_application(self):
        applications.settings.STATIC_DIRS = []
        applications.settings.STATIC_URL = '/static/'
        applications.settings.TORNADO_OPTS = {}

        app = applications.StaticApplication()

        expected = ['/static/(.*)$']
        urls = app.urls
        urls_returned = [u.regex.pattern for u in urls]

        self.assertEqual(urls_returned, expected)


class GetMainApplicationTestCase(AsyncTestCase):

    @patch.object(applications, 'import_module', Mock())
    def test_get_application_urls(self):
        app_name = 'some.web.application'
        app_mock = Mock()

        def aurls():
            return ['/some/url']
        app_mock.urls = aurls()
        expected = app_mock.urls
        with patch('pyrocumulus.web.applications.getattr',
                   create=True) as getmock:
            getmock.return_value = app_mock
            returned_urls = applications._get_application_urls(app_name)

        self.assertEqual(returned_urls, expected)

    @patch.object(applications, 'import_module',
                  Mock(side_effect=ImportError))
    def test_get_application_urls_with_import_error(self):
        app_name = 'some.web.application'
        expected = []
        with patch('pyrocumulus.web.applications.getattr',
                   create=True) as getmock:

            returned_urls = applications._get_application_urls(app_name)

        self.assertEqual(returned_urls, expected)

    @patch.object(applications, 'settings', Mock())
    @patch.object(applications, 'import_module', Mock())
    def test_get_main_application(self):
        some_app = Mock()

        def saurls():
            return [applications.URLSpec('/some/url/(.*)$', Mock())]

        def oaurls():
            return [applications.URLSpec('/other/url/(.*)$', Mock())]

        some_app.urls = saurls()
        other_app = Mock()
        other_app.urls = oaurls()
        applications.settings.APPLICATIONS = ('some.app',
                                              'other.app',)

        with patch('pyrocumulus.web.applications.getattr',
                   create=True) as getmock:
            getmock.side_effect = [some_app, other_app, {}]
            main_app = applications.get_main_application()
            returned = main_app.urls

        expected = some_app.urls + other_app.urls

        self.assertEqual(returned, expected)
