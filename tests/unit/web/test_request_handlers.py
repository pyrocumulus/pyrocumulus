# -*- coding: utf-8 -*-

# Copyright 2013-2016 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
import datetime
import unittest
from unittest import TestCase
from bson.objectid import ObjectId
import json
import tornado

from mock import Mock, patch, MagicMock
from mongomotor import Document, EmbeddedDocument
from mongomotor.fields import (ReferenceField, EmbeddedDocumentField,
                               ListField, IntField, StringField,
                               DateTimeField)
from pyrocumulus.exceptions import PyrocumulusException
from pyrocumulus.db import MongoConnection
from pyrocumulus.utils import fqualname
from pyrocumulus.web import handlers
from pyrocumulus.web.handlers import (ModelHandler,
                                      RestHandler, AuthRestHandler,
                                      ReadOnlyRestHandler,
                                      EmbeddedDocumentHandler,
                                      AuthEmbeddedDocumentHandler,
                                      ReadOnlyEmbeddedDocumentHandler,
                                      StaticFileHandler,
                                      HTTPError)
from pyrocumulus.auth import AccessToken, Permission
from tests import async_test, AsyncMagicMock


class CoroTestHandler(handlers.BasePyroHandler):

    @handlers.decorators.get('')
    @asyncio.coroutine
    def do_get(self):
        return 'GET coro done!'

    @handlers.decorators.get('other')
    def do_something(self):
        pass

    def write(self, c):
        self.chunck = c


class MethTestHandler(handlers.BasePyroHandler):

    @handlers.decorators.get('')
    def do_get(self):
        return 'GET meth done!'

    def write(self, c):
        self.chunck = c


class PyroRequestTestCase(TestCase):

    def test_decode_values(self):
        req = {'a': [b'1', b'2']}
        pyroreq = handlers.PyroRequest(req)
        expected = {'a': ['1', '2']}
        self.assertEqual(pyroreq.new_request, expected)

    def test_get(self):
        req = {'a': [b'1', b'2']}
        pyroreq = handlers.PyroRequest(req)
        self.assertEqual(pyroreq.get('a'), '1')

    def test_getlist(self):
        req = {'a': [b'1', b'2']}
        pyroreq = handlers.PyroRequest(req)
        self.assertEqual(pyroreq.getlist('a'), ['1', '2'])

    def test_delete(self):
        pyroreq = handlers.PyroRequest({'a': [b'1']})
        del pyroreq['a']
        self.assertNotIn('a', pyroreq.keys())

    def test_items(self):
        pyroreq = handlers.PyroRequest({'a': [b'1']})
        self.assertEqual(pyroreq.items(), pyroreq.new_request.items())

    def test_get_item(self):
        pyroreq = handlers.PyroRequest({'a': [b'1']})
        self.assertEqual(pyroreq['a'], pyroreq.new_request['a'])

    def test_setitem(self):
        pyroreq = handlers.PyroRequest({'a': [b'1']})
        pyroreq['b'] = 'bbb'
        self.assertIn('b', pyroreq.keys())


class BasePyroHandlerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection()
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super().setUp()
        application = Mock()
        application.ui_methods = {}
        request = Mock()
        request.arguments = {}

        self.handler = handlers.BasePyroHandler(application, request)
        self.handler.request.headers = self.handler._headers

    @async_test
    def test_getattr_with_validate(self):
        validate_mock = Mock()

        class TestHandler(handlers.BasePyroHandler):

            @asyncio.coroutine
            def validate_and_run(self, operation):
                validate_mock()

        handler = TestHandler(self.handler.application, self.handler.request)
        yield from handler.post('op')
        self.assertTrue(validate_mock.called)

    @async_test
    def test_validate_and_run_no_output(self):

        handler = CoroTestHandler(self.handler.application,
                                  self.handler.request)
        handler.request.method = 'GET'
        self.handler.write_response = MagicMock()
        yield from handler.get('other')
        self.assertFalse(self.handler.write_response.called)

    @async_test
    def test_validate_and_run_with_coro(self):

        handler = CoroTestHandler(self.handler.application,
                                  self.handler.request)
        handler.request.method = 'GET'
        yield from handler.get('')
        self.assertEqual(handler.chunck, 'GET coro done!')

    @async_test
    def test_validate_and_run_with_method(self):

        handler = MethTestHandler(self.handler.application,
                                  self.handler.request)
        handler.request.method = 'GET'
        yield from handler.get('')
        self.assertEqual(handler.chunck, 'GET meth done!')

    def test_validate_options_with_unknown_operation(self):
        with self.assertRaises(HTTPError):
            self.handler.options('does-not-exist')

    @async_test
    def test_validate_options_with_cors(self):
        handler = CoroTestHandler(self.handler.application,
                                  self.handler.request)
        handler.cors_origins = '*'
        handler.options('other')

        self.assertEqual(handler._headers.get(
            "Access-Control-Allow-Origin"), '*')

    def test_validate_options_without_cors(self):
        handler = CoroTestHandler(self.handler.application,
                                  self.handler.request)
        handler.cors_origins = None
        handler.options('other')

        self.assertEqual(handler.request.headers.get(
            "Access-Control-Allow-Origin"), None)

    @patch.object(handlers, 'get_value_from_settings', Mock())
    def test_enable_cors(self):
        handlers.get_value_from_settings.return_value = {
            "Access-Control-Allow-Credentials": 'false'}
        self.handler.cors_origins = '*'
        self.handler._enable_cors()
        self.assertEqual(self.handler._headers.get(
            "Access-Control-Allow-Origin"), '*')
        self.assertEqual(self.handler._headers.get(
            "Access-Control-Allow-Credentials"), 'false')

    def test_initialize_with_cors(self):
        handler = handlers.BasePyroHandler(self.handler.application,
                                           self.handler.request,
                                           **{'cors_origins': '*'})
        handler.request.headers = handler._headers
        self.assertEqual(handler.request.headers.get(
            "Access-Control-Allow-Origin"), '*')

    def test_get_allowed_operations(self):
        handler = CoroTestHandler(self.handler.application,
                                  self.handler.request)
        expected = set(['', 'other'])
        self.assertEqual(expected, handler.get_allowed_operations())

    @async_test
    def test_prepare(self):
        self.assertFalse(self.handler.params)
        yield from self.handler.prepare()

        self.assertTrue(self.handler.params)


class BasePyroAuhHandlerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection()
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super().setUp()
        application = Mock()
        application.ui_methods = {}
        request = Mock()
        request.arguments = {}
        self.handler = handlers.BasePyroAuthHandler(application, request)

    @asyncio.coroutine
    def tearDown(self):
        yield from AccessToken.drop_collection()
        yield from Permission.drop_collection()


    @async_test
    def test_async_prepare_without_api_key(self):
        self.handler.request.headers = {}
        with self.assertRaises(HTTPError):
            yield from self.handler.async_prepare()

    @async_test
    def test_async_prepare_with_good_api_key(self):
        access_token = AccessToken(name='test-token')
        token = yield from access_token.save()
        self.handler.request.headers = {'Authorization': 'token: {}'.format(
            token)}

        yield from self.handler.async_prepare()
        self.assertTrue(self.handler.access_token)

    @async_test
    def test_async_prepare_with_inheritance(self):
        class TestHandler(handlers.BasePyroAuthHandler):
            pass

        handler = TestHandler(self.handler.application,
                              self.handler.request)
        access_token = AccessToken(name='test-token')
        token = yield from access_token.save()
        handler.request.headers = {'Authorization': 'token: {}'.format(
            token)}
        yield from handler.async_prepare()
        self.assertTrue(handler.access_token)

    @async_test
    def test_prepare(self):
        self.handler.async_prepare = AsyncMagicMock()
        yield from self.handler.prepare()
        self.assertTrue(self.handler.async_prepare.called)


class ModelHandlerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection()
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super(ModelHandlerTestCase, self).setUp()

        class Thing(Document):
            name = StringField()

        class TestDocument(Document):
            name = StringField()
            ratting = IntField()
            tags = ListField(StringField())
            thing = ReferenceField(Thing)

        request = Mock()
        request.body = {}
        request.arguments = {'ratting': [b'2'], 'tags': [b'bla', b'ble'],
                             'thing__id': [b'asdfqwrezxvc']}
        kwargs = {'model': TestDocument}
        application = Mock()
        application.ui_methods = {}
        self.thing_class = Thing
        self.thing_instance = Thing()
        self.handler = ModelHandler(application, request, **kwargs)
        self.handler.request.headers = self.handler._headers

    def tearDown(self):
        super(ModelHandlerTestCase, self).tearDown()

    @async_test
    def test_prepare_params(self):
        t = self.thing_class()
        yield from t.save()
        # tests if params are setted correctly
        self.handler.request.arguments = {
            'ratting': [b'2'], 'tags': [b'bla', b'ble'],
            'thing__id': [str(t.id).encode('utf-8')]}

        expected = {'ratting': 2, 'tags': ['bla', 'ble'],
                    'thing': t}
        query, body = yield from self.handler._prepare_params()

        self.assertEqual(query, expected)

        yield from self.thing_class.drop_collection()


class RestHandlerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection()
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    @async_test
    @asyncio.coroutine
    def setUp(self):
        super(RestHandlerTestCase, self).setUp()

        class Ref(Document):
            pass

        class Embed(EmbeddedDocument):
            pass

        class Model(Document):
            a = StringField()
            b = StringField()
            date = DateTimeField()
            ref = ReferenceField(Ref)
            embed = EmbeddedDocumentField(Embed)
            embedlist = ListField(EmbeddedDocumentField(Embed))
            reflist = ListField(ReferenceField(Ref))

        self.model_doc = Model
        self.embed_doc = Embed
        self.ref_doc = Ref

        application = Mock()
        application.ui_methods = {}
        request = Mock()
        request.body = {}
        now = datetime.datetime.now()
        self.now = datetime.datetime(now.year, now.month, now.day,
                                     now.hour, now.minute, now.second)
        request.arguments = {'a': [b'a'], 'b': [b'b'],
                             'date': [self.now.strftime(
                                 '%Y-%m-%d %H:%M:%S').encode('utf-8')]}
        request.method = 'GET'

        self.kwargs = {'model': Model}

        self.handler = RestHandler(application, request, **self.kwargs)
        yield from self.handler.prepare()
        self.handler.write = Mock()

    @async_test
    def tearDown(self):
        yield from self.model_doc.drop_collection()
        yield from self.ref_doc.drop_collection()

    @async_test
    def test_get_object_with_non_existent_object(self):
        d = self.model_doc()
        yield from d.save()
        yield from d.delete()

        self.handler.params = {'id': d.id}
        with self.assertRaises(self.handler.model.DoesNotExist):
            yield from self.handler.get_object()

        yield from self.model_doc.drop_collection()

    @async_test
    def test_get_object(self):
        obj = self.model_doc()
        yield from obj.save()

        self.handler.query = {'id': obj.id}
        obj = yield from self.handler.get_object()
        self.assertTrue(obj)

        yield from self.model_doc.drop_collection()

    @async_test
    def test_insert_object(self):
        self.handler.body = {'a': 'fa', 'b': 'fb'}
        obj = yield from self.handler.insert_object()
        self.assertTrue(obj.id)

    @async_test
    def test_put_object_without_id(self):

        class Req(Document):
            field = StringField(required=True)

        self.handler.initialize(model=Req)
        yield from self.handler.prepare()
        self.handler.params = {'field': 'blablaie'}
        with self.assertRaises(handlers.HTTPError):
            yield from self.handler.put_object()

    @async_test
    def test_put_object_with_unknown_object(self):

        class Req(Document):
            field = StringField(required=True)

        self.handler.initialize(model=Req)
        yield from self.handler.prepare()
        obid = ObjectId()

        self.handler.params = {'id': obid,
                               'field': 'blablaiew'}

        with self.assertRaises(handlers.HTTPError):
            yield from self.handler.put_object()

    @async_test
    def test_put_object(self):
        self.handler.body = {'a': '1'}
        obj = yield from self.handler.insert_object()
        self.handler.body = {'a': '2'}
        obj = yield from self.handler.insert_object()
        self.handler.query = {'id': str(obj.id)}
        self.handler.body = {'a': '2'}
        obj = yield from self.handler.put_object()
        obj = yield from self.handler.model.objects.get(id=obj['id'])
        self.assertTrue(obj.a, '2')

    @async_test
    def test_delete_object_with_non_existent_object(self):
        obj = self.model_doc()
        yield from obj.save()
        yield from obj.delete()

        with self.assertRaises(self.model_doc.DoesNotExist):
            yield from self.handler.delete_object(**{'id': obj.id})

    @async_test
    def test_delete_object(self):
        obj = self.model_doc()
        yield from obj.save()
        self.handler.query = {'id': obj.id}
        yield from self.handler.delete_object()

        with self.assertRaises(self.model_doc.objects._document.DoesNotExist):
            yield from self.model_doc.objects.get(id=obj.id)

    @async_test
    def test_get_with_invalid_operation(self):
        with self.assertRaises(HTTPError):
            yield from self.handler.get('wrong')

    @async_test
    def test_get(self):
        obj = self.model_doc()
        yield from obj.save()
        self.handler.request.arguments = {'id': [str(obj.id).encode('utf-8')]}
        yield from self.handler.prepare()
        self.handler.params = yield from self.handler._prepare_params()
        yield from self.handler._prepare_params()
        yield from self.handler.get('')

        self.assertTrue(self.handler.write.called)

        yield from self.model_doc.drop_collection()

    @async_test
    def test_post_with_invalid_operation(self):
        with self.assertRaises(HTTPError):
            yield from self.handler.post('get')

    @async_test
    def test_put_with_invalid_operation(self):
        with self.assertRaises(HTTPError):
            yield from self.handler.put('delete')

    @async_test
    def test_put(self):
        self.handler.request.method = 'PUT'
        a = self.handler.model(a='a')
        yield from a.save()
        self.handler.query = {'id': str(a.id)}
        self.handler.body = {'b': 'asdf'}
        yield from self.handler.put('')

        self.assertEqual((yield from self.model_doc.objects.count()), 1)

        yield from self.model_doc.drop_collection()

    @async_test
    def test_delete_with_invalid_operation(self):
        with self.assertRaises(HTTPError):
            yield from self.handler.delete('bla')

    @async_test
    def test_delete(self):

        class ToDelete(Document):
            pass

        to_delete = ToDelete()
        yield from to_delete.save()
        to_delete_id = str(to_delete.id)
        del to_delete
        self.handler.request.arguments = {'id': [str(to_delete_id).encode(
            'utf-8')]}
        self.handler.initialize(model=ToDelete)
        self.handler.request.method = 'DELETE'
        yield from self.handler.prepare()
        self.handler.params = yield from self.handler._prepare_params()
        yield from self.handler.delete('')
        yield from asyncio.sleep(0.1)
        with self.assertRaises(ToDelete.objects._document.DoesNotExist):
            yield from ToDelete.objects.get(id=str(to_delete_id))

    def test_get_pagination_without_pagination_args(self):
        self.handler.request.arguments = {}
        expected_pagination = {'ini': 0, 'end': 10, 'max': 10, 'page': 1}

        pagination = self.handler._get_pagination()

        self.assertEqual(expected_pagination, pagination)

    @async_test
    def test_handler_params_with_pagination(self):
        # tests if the params passed to self.models.objects
        # are correctly when pagination args are passed
        self.handler.request.arguments = {}
        yield from self.handler.prepare()
        self.handler.query, b = yield from self.handler._prepare_params()

        self.assertTrue('max' not in self.handler.query.keys() and
                        'page' not in self.handler.query.keys())

    @async_test
    def test_get_pagination_with_pagination_args(self):
        self.handler.request.arguments = {'page': [2], 'max': [30]}
        expected_pagination = {'ini': 30, 'end': 60, 'max': 30, 'page': 2}

        pagination = self.handler._get_pagination()

        self.assertEqual(expected_pagination, pagination)

    @async_test
    def test_get_order_by(self):
        self.handler.request.arguments = {'order_by': ['-date']}
        expected = ['-date']
        order_by = self.handler._get_order_by()
        self.assertEqual(expected, order_by)

    @async_test
    @patch.object(handlers, 'get_converter', Mock())
    def test_write_response_with_extra_args(self):
        # tests if the json_extra_params are include correctly in the
        # json response
        @asyncio.coroutine
        def method_to_call(inst, **kwargs):
            return {'arg': 1}

        self.handler.params = yield from self.handler._prepare_params()
        self.handler.json_extra_params = {'some': 1}
        yield from self.handler.write_response({'arg': 1})

        called = self.handler.write.call_args[0]
        expected = ({'some': 1, 'arg': 1},)
        self.assertEqual(called, expected)

    @async_test
    def test_write_response_with_string(self):
        yield from self.handler.write_response("blabla")
        called = self.handler.write.call_args[0][0]
        self.assertEqual(called, 'blabla')

    @async_test
    def test_get_clean_dict_with_list(self):
        class Doc(Document):
            pass

        l = [Doc()]
        r = yield from self.handler._get_clean_dict(l)
        self.assertEqual(len(r['items']), 1)

    @async_test
    def test_get_clean_dict_with_wierd_thing(self):
        class Wierd(object):
            pass

        returned_obj = Wierd()
        with self.assertRaises(handlers.PyrocumulusConfusionError):
            yield from self.handler._get_clean_dict(returned_obj)

    @async_test
    def test_show_documentation(self):
        doc = self.handler.show_documentation()
        self.assertTrue(doc.startswith(self.handler.model.__name__))

    @async_test
    def test_get_or_list_with_pk(self):
        get_object = Mock()

        @asyncio.coroutine
        def go():
            get_object()

        self.handler.get_object = go
        self.handler.query = {'id': 'some-id'}
        yield from self.handler.get_or_list()

        self.assertTrue(get_object.called)

    @async_test
    def test_get_or_list_without_pk(self):
        list_objects = Mock()

        @asyncio.coroutine
        def lo():
            list_objects()

        self.handler.list_objects = lo
        self.handler.params = {'some-thing': 'bla'}
        yield from self.handler.get_or_list()

        self.assertTrue(list_objects.called)

    @async_test
    def test_list_objects(self):
        # tests if total_itens is setted correctly to json_extra_params
        d1, d2 = self.model_doc(), self.model_doc()
        yield from d1.save()
        yield from d2.save()

        self.handler.query = {}
        yield from self.handler.list_objects()

        self.assertEqual(self.handler.json_extra_params['total_items'], 2)

        yield from self.model_doc.drop_collection()

    @async_test
    def test_get_list_queryset(self):
        d1, d2 = self.model_doc(a='a'), self.model_doc(a='z')
        yield from d1.save()
        yield from d2.save()

        self.handler.order_by = [b'-a']

        qs = self.handler.get_list_queryset(**{})
        self.assertEqual((yield from qs[0]), d2)

    @async_test
    def test_list_objects_with_a_list_in_kwargs(self):
        # tests if kwargs are cleaned. No list can be passed
        # to filter()
        self.handler.model = Mock()
        kwargs = {'a': 1, 'b': ['some']}
        expected = {'a': 1, 'b': 'some'}
        objlist = self.model_doc.objects

        self.handler.model.objects.filter.return_value = objlist
        self.handler.query = kwargs
        yield from self.handler.list_objects()
        called = self.handler.model.objects.filter.call_args[1]

        self.assertEqual(expected, called)

    @async_test
    def test_list_objects_pagination(self):
        # tests if the pagination (a slice) works ok
        # tests if total_itens is setted correctly to json_extra_params
        d1, d2 = self.model_doc(), self.model_doc()
        yield from d1.save()
        yield from d2.save()
        self.handler.pagination = {'ini': 0, 'end': 1}
        self.handler.query = {}
        returned_objects = yield from self.handler.list_objects()

        self.assertEqual((yield from returned_objects.count()), 1)

    @async_test
    def test_list_objects_with_order_by(self):
        # tests if order_by is called
        objlist = MagicMock()

        @asyncio.coroutine
        def c(*a, **kw):
            return 1

        objlist.order_by.return_value.__getitem__ = c
        objlist.order_by.return_value.count = c
        self.handler.model = Mock()
        self.handler.model.objects.filter.return_value = objlist
        self.handler.order_by = [b'-date']
        yield from self.handler.list_objects(**{})

        called = objlist.order_by.call_args[0]
        expected = ('-date',)
        self.assertEqual(called, expected)

    def test_query_has_pk(self):
        self.handler.query = {'id': 'some-id'}
        self.assertTrue(self.handler._query_has_pk())

    def test_query_has_pk_without_pk(self):
        self.handler.query = {}
        self.assertFalse(self.handler._query_has_pk())

    @async_test
    def test_embeddedhandler(self):

        self.assertEqual(type(self.handler).embeddedhandler(),
                         EmbeddedDocumentHandler)

    @async_test
    def test_send_options(self):
        self.handler.prepare()
        yield from self.handler._prepare_params()
        self.handler.send_options()
        expected = 'GET, POST, PUT, DELETE, OPTIONS'
        allowed_methods = self.handler.request.headers.get(
            'GET, POST, PUT, DELETE, OPTIONS')
        self.assertTrue(expected, allowed_methods)


class ReadOnlyRestHandlerTestCase(TestCase):

    def setUp(self):
        super(ReadOnlyRestHandlerTestCase, self).setUp()

        Model = Mock()
        request = Mock()
        request.arguments = {}
        application = Mock()
        application.ui_methods = {}
        kwargs = {'model': Model}

        self.handler = ReadOnlyRestHandler(application, request, **kwargs)

    def test_post(self):
        with self.assertRaises(HTTPError):
            self.handler.post('list')

    def test_put(self):
        with self.assertRaises(HTTPError):
            self.handler.put('put')

    def test_delete(self):
        with self.assertRaises(HTTPError):
            self.handler.delete('delete')


class AuthRestHandlerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection()
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super().setUp()

        class TestDoc(Document):
            bla = StringField()
            uniq = StringField(unique=True)

        request = Mock()
        request.arguments = {}
        request.body = {}
        request.method = 'get'
        kwargs = {'model': TestDoc}
        application = Mock()
        application.ui_methods = {}
        self.handler = handlers.AuthRestHandler(application, request, **kwargs)
        self.handler.request.headers = self.handler._headers

        self.test_doc = TestDoc
        self.access_token = AccessToken()

    @async_test
    def tearDown(self):
        yield from AccessToken.drop_collection()
        yield from Permission.drop_collection()
        yield from self.test_doc.drop_collection()
        super().tearDown()

    @async_test
    def test_insert_object(self):
        token = yield from self.access_token.save()
        yield from Permission.create_perms_to(self.access_token,
                                              self.test_doc, 'crud')
        self.handler.request.body = json.dumps(
            {'bla': 'blabla'}).encode('utf-8')
        self.handler.request.method = 'POST'
        self.handler.request.headers = {'Authorization': 'token: {}'.format(
            token)}

        yield from self.handler.prepare()
        yield from self.handler.post('')

        cnt = yield from self.test_doc.objects.all().count()
        self.assertEqual(cnt, 1)


class EmbeddedDocumentHandlerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection()
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super(EmbeddedDocumentHandlerTestCase, self).setUp()

        class Model(EmbeddedDocument):
            pass

        class ModelToList(EmbeddedDocument):
            pass

        class Parent(Document):
            embed = EmbeddedDocumentField(Model)
            embedlist = ListField(EmbeddedDocumentField(ModelToList))

        self.parent_doc = Parent
        self.model = Model
        self.listmodel = ModelToList
        application = Mock()
        application.ui_methods = {}
        request = Mock()
        request.arguments = {}
        request.body = {}
        kwargs = {'parent_doc': Parent, 'model': Model}

        self.handler = EmbeddedDocumentHandler(application, request, **kwargs)

        kwargs = {'parent_doc': Parent, 'model': ModelToList}
        # handler with an EmbeddedDocument in a ListField
        self.list_handler = EmbeddedDocumentHandler(
            application, request, **kwargs)

    @async_test
    def tearDown(self):
        yield from self.parent_doc.drop_collection()
        super(EmbeddedDocumentHandlerTestCase, self).tearDown()

    @async_test
    def test_prepare_without_parent_id_arg(self):
        # tests if raises if no parent_id on request args
        self.handler.request.arguments = {}
        self.handler.initialize(parent_doc=self.parent_doc, model=self.model)
        with self.assertRaises(HTTPError):
            yield from self.handler.prepare()

    @async_test
    def test_prepare(self):
        # test if parent_id is setted correctly
        parent = self.parent_doc()
        yield from parent.save()
        pid = str(parent.id).encode('utf-8')
        self.handler.request.arguments = {'parent_id': [pid]}
        yield from self.handler.prepare()
        self.assertEqual(self.handler.parent_id, pid.decode())

    @async_test
    def test_get_field_name_in_field_list(self):
        # tests if the name for the field is
        # returned correctly when its in a
        # ListField
        parent_inst = self.parent_doc()
        yield from parent_inst.save()
        self.handler.request.arguments = {'parent_id': [str(
            parent_inst.id).encode('utf-8')]}
        self.handler.initialize(self.parent_doc, self.listmodel)
        yield from self.handler.prepare()

        expected = 'embedlist'
        returned = self.handler._get_field_name()

        self.assertEqual(expected, returned)

    @async_test
    def test_put_object_in_a_list_field(self):
        # tests _put_object when the EmbeddedDocument is
        # in a ListField and the ListField is not empty
        parent_inst = self.parent_doc()
        d = self.listmodel()
        parent_inst.embedlist = [d]
        yield from parent_inst.save()
        self.handler.request.arguments = {
            'parent_id': [str(parent_inst.id).encode('utf-8')]}

        self.handler.initialize(self.parent_doc, self.listmodel)
        yield from self.handler.prepare()
        yield from self.handler.put_object(**{})

        parent_inst = yield from self.parent_doc.objects.get(id=parent_inst.id)
        self.assertEqual(len(parent_inst.embedlist), 2)

        yield from self.parent_doc.drop_collection()

    @async_test
    def test_put_object_in_an_empty_list_field(self):
        # tests _put_object when the EmbeddedDocument is
        # in a ListField and the ListField is empty
        parent_inst = self.parent_doc()
        yield from parent_inst.save()
        self.handler.request.arguments = {
            'parent_id': [str(parent_inst.id).encode('utf-8')]}
        self.handler.request.body = json.dumps({}).encode('utf-8')

        self.handler.initialize(self.parent_doc, self.listmodel)
        yield from self.handler.prepare()
        yield from self.handler.put_object(**{})

        parent_inst = yield from self.parent_doc.objects.get(id=parent_inst.id)
        self.assertEqual(len(parent_inst.embedlist), 1)

        yield from self.parent_doc.drop_collection()

    @async_test
    def test_put_object(self):
        # tests _put_object  when the EmbeddedDocument is
        # in a EmbeddedDocumentField.
        # here, setattr must set embed_mock as the value
        parent_inst = self.parent_doc()
        yield from parent_inst.save()
        self.handler.request.arguments = {
            'parent_id': [str(parent_inst.id).encode('utf-8')]}

        self.handler.initialize(self.parent_doc, self.model)
        yield from self.handler.prepare()
        self.handler.params = {}
        obj = yield from self.handler.put_object()

        self.assertTrue(isinstance(obj, EmbeddedDocument))

        yield from self.parent_doc.drop_collection()

    @async_test
    def test_list_objects(self):
        # Test if the objects in the ListField
        # are returned correctly
        parent_inst = self.parent_doc()
        list_model = parent_inst.embedlist
        for i in range(3):
            list_model.append(self.listmodel())

        parent_inst.embedlist = list_model
        yield from parent_inst.save()
        self.handler.request.arguments = {
            'parent_id': [str(parent_inst.id).encode('utf-8')]}
        self.handler.initialize(self.parent_doc, self.listmodel)
        yield from self.handler.prepare()
        self.handler.params = yield from self.handler._prepare_params()

        returned = yield from self.handler.list_objects(**{'a': 1})

        self.assertEqual(len(returned), 3)

        yield from self.parent_doc.drop_collection()

    @async_test
    def test_list_objects_with_pagination(self):
        # Test if the objects in the ListField
        # are returned correctly
        parent_inst = self.parent_doc()
        parent_inst.embedlist = [self.listmodel(), self.listmodel(),
                                 self.listmodel()]
        yield from parent_inst.save()
        self.handler.request.arguments = {
            'parent_id': [str(parent_inst.id).encode('utf-8')]}
        self.handler.initialize(self.parent_doc, self.listmodel)
        yield from self.handler.prepare()
        self.handler.pagination = {'ini': 0, 'end': 2}

        returned = yield from self.handler.list_objects(**{'a': 1})

        self.assertEqual(len(returned), 2)

        yield from self.parent_doc.drop_collection()

    @async_test
    def test_get_field_name(self):
        # tests if the name for the field is
        # returned correctly when its in a
        # EmbeddedDocumentField
        parent_inst = self.parent_doc()
        yield from parent_inst.save()
        self.handler.request.arguments = {'parent_id': [str(
            parent_inst.id).encode('utf-8')]}
        self.handler.initialize(self.parent_doc, self.model)
        yield from self.handler.prepare()

        expected = 'embed'

        returned = self.handler._get_field_name()
        self.assertEqual(expected, returned)

        yield from self.parent_doc.drop_collection()

    def test_embeddedhandler(self):
        self.assertEqual(type(self.handler).embeddedhandler(),
                         type(self.handler))


class StaticFileHandlerTestCase(TestCase):

    def setUp(self):
        super(StaticFileHandlerTestCase, self).setUp()

        application = Mock()
        application.ui_methods = {}
        request = Mock()
        kwargs = {'static_dirs': ['/some/dir', '/other/dir']}

        self.handler = StaticFileHandler(application, request, **kwargs)

    @patch.object(handlers.os.path, 'exists', Mock())
    def test_get_absolute_path(self):
        handlers.os.path.exists.return_value = True
        expected = '/some/dir/some/file.js'
        returned = StaticFileHandler.get_absolute_path('', 'some/file.js')

        self.assertEqual(expected, returned)

    @patch.object(handlers.os.path, 'exists', Mock())
    def test_get_absolute_path_without_static_dirs(self):
        with self.assertRaises(handlers.StaticFileError):
            StaticFileHandler.static_dirs = []
            StaticFileHandler.get_absolute_path('', 'some/file.js')

    @patch.object(handlers.os.path, 'exists', Mock())
    @patch.object(handlers.os.path, 'isfile', Mock())
    def test_validade_absolute_path(self):
        handlers.os.path.exists.return_value = True
        handlers.os.path.isfile.return_value = True
        self.handler.validate_absolute_path('', '/some/dir/some/file.js')

        self.assertEqual(self.handler.root, '/some/dir')


class TemplateHandlerTestCase(unittest.TestCase):

    def setUp(self):
        super(TemplateHandlerTestCase, self).setUp()

        application = Mock()
        application.ui_methods = {}
        request = Mock()
        self.handler = handlers.TemplateHandler(application, request)

    def test_prepare(self):
        self.handler.request.arguments = {'a': [b'a', b'b']}
        self.handler.prepare()
        self.assertEqual(self.handler.params.getlist('a'), ['a', 'b'])

    @patch.object(handlers, 'render_template', Mock())
    @patch.object(handlers.TemplateHandler, 'write', Mock())
    def test_render_template(self):
        template = 'bla.html'
        extra_context = {'some': 'thing'}
        expected = (template, self.handler.request, extra_context)

        self.handler.render_template(template, extra_context)
        called = handlers.render_template.call_args[0]

        self.assertEqual(called, expected)


class GetRestHandlerTestCase(TestCase):

    def test_get_rest_handler_with_document(self):
        class Doc(Document):
            pass

        returned = handlers.get_rest_handler(Doc)

        self.assertEqual(returned, RestHandler)

    def test_get_rest_handler_with_embedded_document(self):
        class Embed(EmbeddedDocument):
            pass

        class Parent(Document):
            pass

        returned = handlers.get_rest_handler(Embed, Parent)

        self.assertEqual(returned, EmbeddedDocumentHandler)

    def test_get_rest_handler_with_embedded_document_without_parent(self):
        class Embed(EmbeddedDocument):
            pass

        class Parent(Document):
            pass

        with self.assertRaises(PyrocumulusException):
            returned = handlers.get_rest_handler(Embed)

    def test_get_rest_handler_with_unknown_object(self):
        with self.assertRaises(PyrocumulusException):
            handlers.get_rest_handler(Mock)

    def test_get_rest_handler_auth(self):
        class Doc(Document):
            pass

        returned = handlers.get_rest_handler(Doc, auth=True)

        self.assertEqual(returned, AuthRestHandler)

    def test_get_rest_handler_read_only(self):
        class Doc(Document):
            pass

        returned = handlers.get_rest_handler(Doc, readonly=True)

        self.assertEqual(returned, ReadOnlyRestHandler)

    @patch.object(handlers, 'get_value_from_settings', Mock())
    def test_get_rest_handler_with_custom_default_handler(self):

        class Doc(Document):
            pass

        handlers.get_value_from_settings.side_effect = [{},
                                                        fqualname(FakeHandler)]
        returned = handlers.get_rest_handler(Doc)
        self.assertEqual(returned, FakeHandler)

    @patch.object(handlers, 'get_value_from_settings', Mock())
    def test_get_rest_handler_with_custom_class_handler(self):

        class Doc(Document):
            pass

        handlers.get_value_from_settings.side_effect = [
            {fqualname(Doc): fqualname(FakeHandler)}, None]

        returned = handlers.get_rest_handler(Doc)
        self.assertEqual(returned, FakeHandler)

    def test_get_rest_handler_with_embedded_document_auth(self):
        class Embed(EmbeddedDocument):
            pass

        class Parent(Document):
            pass

        returned = handlers.get_rest_handler(Embed, Parent, auth=True)

        self.assertEqual(returned, AuthEmbeddedDocumentHandler)

    def test_get_rest_handler_with_embedded_document_readonly(self):
        class Embed(EmbeddedDocument):
            pass

        class Parent(Document):
            pass

        returned = handlers.get_rest_handler(Embed, Parent, readonly=True)

        self.assertEqual(returned, ReadOnlyEmbeddedDocumentHandler)

    @patch.object(handlers, 'get_value_from_settings', Mock())
    def test_get_rest_handler_with_embedded_document_with_custom_handler(self):
        class Embed(EmbeddedDocument):
            pass

        class Parent(Document):
            pass

        handlers.get_value_from_settings.side_effect = [
            {}, fqualname(FakeHandler)]

        returned = handlers.get_rest_handler(Embed, Parent)

        self.assertEqual(returned, FakeHandler)

    @patch.object(handlers, 'get_value_from_settings', Mock())
    def test_get_rest_handler_with_embedded_document_with_class_handler(self):
        class Embed(EmbeddedDocument):
            pass

        class Parent(Document):
            pass

        handlers.get_value_from_settings.side_effect = [
            {fqualname(Embed): fqualname(FakeHandler)}, None]

        returned = handlers.get_rest_handler(Embed, Parent)

        self.assertEqual(returned, FakeHandler)


# just for test
class FakeHandler:
    pass


# making coverage happy
import warnings
with warnings.catch_warnings():
    warnings.filterwarnings('ignore')
    from pyrocumulus.web import request_handlers
