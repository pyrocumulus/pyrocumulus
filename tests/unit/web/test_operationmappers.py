# -*- coding: utf-8 -*-

# Copyright 2016 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus. If not, see <http://www.gnu.org/licenses/>.

from copy import copy
from unittest import TestCase
from tornado.web import HTTPError
from pyrocumulus.web.operationmappers import HandlerOperationMapper


class HandlerOperationMapperTestCase(TestCase):

    def setUp(self):
        super().setUp()

        self._old_allowed = copy(HandlerOperationMapper._allowed_operations)
        self.cls_fqualname = 'tests.unit.web.test_urlmappers.TestHandler'

    def tearDown(self):
        HandlerOperationMapper._allowed_operations = self._old_allowed

    def test_create_allowed_operations(self):
        HandlerOperationMapper.create_allowed_operations(self.cls_fqualname)

        expected = ['get', 'post', 'patch', 'put', 'delete', 'options',
                    'inherited']
        keys = list(HandlerOperationMapper._allowed_operations[
            self.cls_fqualname].keys())
        expected.sort(), keys.sort()
        self.assertEqual(expected, keys)

    def test_get_allowed_operations_for(self):
        expected_keys = ['get', 'post', 'patch', 'put', 'delete', 'options',
                         'inherited']

        HandlerOperationMapper.create_allowed_operations(self.cls_fqualname)
        allowed = HandlerOperationMapper.get_allowed_operations_for(
            self.cls_fqualname)
        keys = allowed.keys()

        self.assertEqual(sorted(expected_keys), sorted(keys))

    def test_set_operation(self):
        HandlerOperationMapper.set_operation(self.cls_fqualname, 'get',
                                             'some-op', lambda self: None)

        allowed = HandlerOperationMapper.get_allowed_operations_for(
            self.cls_fqualname)

        self.assertIn('some-op', allowed['get'].keys())

    def test_set_operation_with_unknown_req_method(self):
        with self.assertRaises(KeyError):
            HandlerOperationMapper.set_operation(self.cls_fqualname, 'wierdo!',
                                                 'some-op', lambda self: None)

    def test_update_operations(self):
        ops = {'get': {'some-op': lambda self: None},
               'post': {'some-op': lambda self: None}}

        HandlerOperationMapper.update_operations_for(self.cls_fqualname, ops)

        allowed = HandlerOperationMapper.get_allowed_operations_for(
            self.cls_fqualname)
        self.assertIn('some-op', allowed['get'].keys())

    def test_update_operations_for_already_existing_ops(self):
        ops = {'get': {'some-op': lambda self: None},
               'post': {'some-op': lambda self: None}}
        HandlerOperationMapper.create_allowed_operations(self.cls_fqualname)
        HandlerOperationMapper.set_operation(
            self.cls_fqualname, 'get', '', lambda self: None)
        HandlerOperationMapper.update_operations_for(self.cls_fqualname, ops)

        allowed = HandlerOperationMapper.get_allowed_operations_for(
            self.cls_fqualname)
        self.assertIn('', allowed['get'].keys())

    def test_get_all_operations_for(self):
        ops = {'get': {'some-op': lambda self: None},
               'post': {'some-op': lambda self: None},
               'delete': {'other-op': lambda self: None}}

        HandlerOperationMapper.update_operations_for(self.cls_fqualname, ops)
        expected = set(['some-op', 'other-op'])

        returned = HandlerOperationMapper.get_all_operations_for(
            self.cls_fqualname)

        self.assertEqual(expected, returned)

    def test_validate_with_unknown_operation(self):
        with self.assertRaises(HTTPError):
            HandlerOperationMapper.validate(self.cls_fqualname, 'get', 'sauci')

    def test_validate_with_not_allowed_method(self):
        HandlerOperationMapper.set_operation(self.cls_fqualname, 'get',
                                             'bla', lambda self: None)
        with self.assertRaises(HTTPError):
            HandlerOperationMapper.validate(self.cls_fqualname, 'post', 'bla')

    def test_validate_ok(self):
        HandlerOperationMapper.set_operation(self.cls_fqualname, 'get',
                                             'bla', lambda self: None)

        meth, args, kwargs = HandlerOperationMapper.validate(
            self.cls_fqualname, 'get', 'bla')
        self.assertTrue(callable(meth))

    def test_validate_ok_regex_args(self):
        HandlerOperationMapper.set_operation(self.cls_fqualname, 'get',
                                             'bla/(\d+)', lambda self: None)

        meth, args, kwargs = HandlerOperationMapper.validate(
            self.cls_fqualname, 'get', 'bla/10')
        self.assertTrue(callable(meth))
        self.assertEqual(args, [b'10'])

    def test_validate_ok_regex_kwargs(self):
        HandlerOperationMapper.set_operation(self.cls_fqualname, 'get',
                                             'bla/(?P<field>\d+)',
                                             lambda self: None)

        meth, args, kwargs = HandlerOperationMapper.validate(
            self.cls_fqualname, 'get', 'bla/10')
        self.assertTrue(callable(meth))
        self.assertEqual(kwargs, {'field': b'10'})
