# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from tornado.testing import AsyncTestCase
from mock import Mock, patch
from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import EmbeddedDocumentField
from pyrocumulus.web import urlmappers
from pyrocumulus.web.urlmappers import (DocumentURLMapper,
                                        EmbeddedDocumentURLMapper)


class DocumentURLMapperTestCase(AsyncTestCase):

    def setUp(self):
        super(DocumentURLMapperTestCase, self).setUp()

        class Embed(EmbeddedDocument):
            pass

        class Doc(Document):
            embeded = EmbeddedDocumentField(Embed)

        request_handler = Mock()
        self.urlmapper = DocumentURLMapper(Doc, request_handler)

    def test_get_url_pattern(self):
        expected = r'doc/(.*)'
        returned = self.urlmapper.get_url_pattern()

        self.assertEqual(expected, returned)

    def test_get_url_pattern_with_prefix(self):
        expected = r'prefix/doc/(.*)'
        returned = self.urlmapper.get_url_pattern(prefix='prefix')

        self.assertEqual(expected, returned)

    def test_get_url_pattern_with_embedded_document_and_prefix(self):
        self.urlmapper.url_prefix = 'prefix'
        expected = r'prefix/doc/embeded/(.*)$'
        returned = self.urlmapper.urls[0].regex.pattern

        self.assertEqual(expected, returned)

    def test_urls(self):
        expected = [r'doc/embeded/(.*)$', r'doc/(.*)$']

        urls = self.urlmapper.urls
        returned = [u.regex.pattern for u in urls]

        self.assertEqual(expected, returned)

    def test_get_url_name(self):
        expected = 'url.tests.unit.web.test_urlmappers.Doc'
        returned = self.urlmapper.get_url_name()

        self.assertEqual(expected, returned)

    @patch.object(urlmappers, 'settings', Mock())
    def test_get_handler_kwargs(self):
        urlmappers.settings.CORS_ORIGINS = None
        self.urlmapper.document.REST_OBJECT_DEPTH = 2
        expected = dict(model=self.urlmapper.document, object_depth=2)
        returned = self.urlmapper.get_handler_kwargs()

        self.assertEqual(expected, returned)

    @patch.object(urlmappers, 'settings', Mock())
    def test_get_handler_kwargs_with_cors(self):
        urlmappers.settings.CORS_ORIGINS = '*'
        expected = dict(model=self.urlmapper.document,
                        cors_origins='*', object_depth=1)
        returned = self.urlmapper.get_handler_kwargs()

        self.assertEqual(expected, returned)


class EmbeddedDocumentURLMapperTestCase(unittest.TestCase):

    def setUp(self):
        class Embed(EmbeddedDocument):
            pass

        class Doc(Document):
            embeded = EmbeddedDocumentField(Embed)

        request_handler = Mock()
        self.urlmapper = EmbeddedDocumentURLMapper(Embed, Doc, request_handler)

    def test_get_handler_kwargs(self):
        expected = dict(model=self.urlmapper.document,
                        parent_doc=self.urlmapper.parent_doc)
        returned = self.urlmapper.get_handler_kwargs()

        self.assertEqual(expected, returned)

    def test_get_url_name(self):
        expected = 'url.tests.unit.web.test_urlmappers.Doc.Embed'
        returned = self.urlmapper.get_url_name()

        self.assertEqual(expected, returned)
