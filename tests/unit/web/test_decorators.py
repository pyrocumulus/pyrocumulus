# -*- coding: utf-8 -*-

# Copyright 2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus. If not, see <http://www.gnu.org/licenses/>.

from copy import copy
from mock import Mock
import tornado
from tornado import gen
from tornado.testing import AsyncTestCase, gen_test
from tornado.web import HTTPError
from pyrocumulus.web import decorators
from pyrocumulus.web.handlers import BasePyroHandler


class TestHandler(BasePyroHandler):

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self._resp = None

    def write(self, ch):
        self._resp = ch

    @decorators.get('bla')
    @gen.coroutine
    def get_bla(self, **kwargs):
        return {'bla': 1}

    @decorators.post('bla')
    def post_bla(self, **kwargs):
        return {'bla': 1}

    @decorators.put('ble')
    @gen.coroutine
    def put_bla(self, **kwargs):
        return {'ble': 'uhu!'}

    @decorators.delete('bli')
    def delete_bla(self, **kwargs):
        return {'bli': 'foi!'}

    @decorators.patch('blo')
    def patch_blo(self, **kwargs):
        return {'blo': 'updated!'}


class DecoratorsTest(AsyncTestCase):

    def setUp(self):
        super().setUp()
        request = Mock()
        request.arguments = {}
        application = Mock()
        application.ui_methods = {}
        self.old_allowed = copy(
            decorators.HandlerOperationMapper._allowed_operations)

        self.handler = TestHandler(application, request)

        self.cls_fqualname = 'tests.unit.web.test_decorators.TestHandler'

    def tearDown(self):
        super().tearDown()
        decorators.HandlerOperationMapper.\
            _allowed_operations = self.old_allowed

    def test_get_class_fqualname_from_method(self):
        returned = decorators._get_class_fqualname_from_method(
            TestHandler.get_bla)
        self.assertEqual(self.cls_fqualname, returned)

    @gen_test
    def test_get(self):

        self.handler.request.method = 'GET'
        yield self.handler.get('bla')

        self.assertTrue(self.handler._resp)

    @gen_test
    def test_get_with_unknown_operation(self):
        self.handler.request.method = 'GET'

        with self.assertRaises(HTTPError):
            yield self.handler.get('blabla')

    @gen_test
    def test_post(self):
        self.handler.request.method = 'POST'
        yield self.handler.post('bla')

        self.assertTrue(self.handler._resp)

    @gen_test
    def test_post_with_not_allowed_operation(self):
        self.handler.request.method = 'POST'
        with self.assertRaises(HTTPError):
            yield self.handler.post('ble')

    @gen_test
    def test_put(self):
        self.handler.request.method = 'PUT'

        yield self.handler.put('ble')

        self.assertTrue(self.handler._resp)

    @gen_test
    def test_delete(self):

        self.handler.request.method = 'DELETE'
        yield self.handler.delete('bli')

        self.assertTrue(self.handler._resp)

    @gen_test
    def test_patch(self):
        self.handler.request.method = 'PATCH'
        yield self.handler.patch('blo')

        self.assertTrue(self.handler._resp)
