# -*- coding: utf-8 -*-

# Copyright 2016 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus. If not, see <http://www.gnu.org/licenses/>.

from unittest import TestCase
from unittest.mock import Mock
from pyrocumulus.utils import fqualname
from pyrocumulus.web.decorators import get, post
from pyrocumulus.web.handlers import BasePyroHandler
from pyrocumulus.web.metaclasses import HandlerOperationMetaclass


class TestHandler(BasePyroHandler, metaclass=HandlerOperationMetaclass):

    @get('bla')
    def get_bla(self):
        return 'bla'


class NoInstHandler(TestHandler):
    pass


class OtherHandler(NoInstHandler):

    @post('ble')
    def post_ble(self):
        pass


class OverwritesIt(OtherHandler):

    @get('bla')
    def other_bla(self):
        return 'other bla'


class HandlerOperationMetaclassTestCase(TestCase):

    def test_initial_operations(self):
        application = Mock()
        application.ui_methods = {}
        request = Mock()
        t = TestHandler(application, request)
        self.assertIn('bla', t.get_allowed_operations())

    def test_inherited_operations(self):
        application = Mock()
        application.ui_methods = {}
        request = Mock()
        t = OtherHandler(application, request)
        self.assertIn('', t.get_allowed_operations())

    def test_overwrited_operations(self):
        application = Mock()
        application.ui_methods = {}
        request = Mock()
        t = OverwritesIt(application, request)

        call_meth = t.operation_mapper.get_allowed_operations_for(
            fqualname(OverwritesIt))['get']['bla']
        self.assertEqual(call_meth(Mock()), 'other bla')
