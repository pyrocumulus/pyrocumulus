# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from mock import patch, Mock
from pyrocumulus.web import template


@patch.object(template.os.path, 'exists', Mock())
class TemplateFinderTestCase(unittest.TestCase):
    @patch.object(template, 'settings', Mock())
    def setUp(self):
        template.settings.TEMPLATE_DIRS = ['/some/dir/', '/some/other/dir']
        self.finder = template.TemplateFinder()

    def test_find_template(self):
        template.os.path.exists.side_effect = [False, True]

        template_name = 'my_template.html'
        expected = '/some/other/dir/my_template.html'
        returned = self.finder.find_template(template_name)

        self.assertEqual(expected, returned)

    def test_find_template_not_found(self):
        template.os.path.exists.side_effect = [False, False]
        template_name = 'my_template.html'

        with self.assertRaises(template.TemplateNotFound):
            self.finder.find_template(template_name)


class ContextManagerTestCase(unittest.TestCase):
    def setUp(self):
        request = Mock()
        self.manager = template.ContextManager(request)

    @patch.object(template, 'settings', Mock())
    def test_get_context_processors(self):
        context_processors = ['pyrocumulus.web.template.ContextProcessor']
        template.settings.CONTEXT_PROCESSORS = context_processors
        processors = self.manager.get_context_processors()

        self.assertEqual(template.ContextProcessor, processors[0].__class__)

    @patch.object(template, 'settings', Mock())
    def test_get_context(self):
        context_processors = ['pyrocumulus.web.template.ContextProcessor']
        template.settings.CONTEXT_PROCESSORS = context_processors
        request = Mock()

        expected = template.ContextProcessor(request).get_context()
        returned = self.manager.get_context()
        self.assertEqual(expected, returned)


class ContextProcessorTestCase(unittest.TestCase):
    @patch.object(template, 'settings', Mock())
    def test_get_context(self):
        template.settings.STATIC_URL = '/static/'
        expected = {'static_url': '/static/'}

        request = Mock()
        processor = template.ContextProcessor(request)
        returned = processor.get_context()
        self.assertEqual(expected, returned)


class RenderTemplateTestCase(unittest.TestCase):

    @patch.object(template, 'settings', Mock())
    @patch.object(template, 'Loader', Mock())
    @patch.object(template, 'TemplateFinder', Mock())
    def test_render_template(self):
        templatename = 'some/template.html'
        extra_context = {'api_url': 'http://some.where.com/api'}

        # mocking all things
        context_processors = ['pyrocumulus.web.template.ContextProcessor']
        template.settings.CONTEXT_PROCESSORS = context_processors
        template.settings.STATIC_URL = '/static/'

        finder_mock = Mock()
        finder_mock.find_template.return_value = '/my/path/to/%s' % (
            templatename)
        template.TemplateFinder.return_value = finder_mock

        loader_mock = Mock()
        template.Loader.return_value.load = loader_mock

        generate_mock = Mock()
        loader_mock.return_value.generate = generate_mock

        # calling it!
        template.render_template(templatename, extra_context=extra_context)

        fileloaded = loader_mock.call_args[0][0]
        context_called = generate_mock.call_args[1]

        self.assertEqual(fileloaded, 'template.html')
        self.assertEqual(context_called,
                         {'api_url': 'http://some.where.com/api',
                          'static_url': '/static/'})
