# -*- coding: utf-8 -*-

# Copyright 2013-2015 Juca Crispim <juca@poraodojuca.net>

# This file is part of pyrocumulus.

# pyrocumulus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# pyrocumulus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pyrocumulus. If not, see <http://www.gnu.org/licenses/>.

from unittest import TestCase
from unittest.mock import Mock
from mongomotor import Document
import tornado
from pyrocumulus.auth import AccessToken, Permission
from pyrocumulus.db import MongoConnection
from pyrocumulus.web import mixins, handlers
from pyrocumulus.web.handlers import HTTPError
from tests import async_test


class BasicAPIKeyAuthMixinTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection()
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super().setUp()

        class TestAuthHandler(mixins.BasicAPIKeyAuthMixin,
                              handlers.BasePyroHandler):
            pass

        application = Mock()
        application.ui_methods = {}
        self.request = Mock()
        self.request.arguments = {}
        self.handler_cls = TestAuthHandler
        handler = self.handler_cls(application, self.request)
        self.handler = handler

    @async_test
    def tearDown(self):
        yield from AccessToken.drop_collection()
        yield from Permission.drop_collection()
        super().tearDown()

    def test_get_domain_without_domain(self):
        domain = self.handler._get_domain(None)
        self.assertFalse(domain)

    @async_test
    def test_async_prepare_without_api_key(self):
        self.request.headers = {}
        with self.assertRaises(HTTPError):
            yield from self.handler.async_prepare()

    @async_test
    def test_async_prepare_options(self):
        application = Mock()
        application.ui_methods = {}
        self.request.method = 'OPTIONS'
        self.request.headers = {'Referer': 'http://nada.com'}
        self.handler._get_token = Mock()
        self.handler.check_perms = Mock()

        yield from self.handler.async_prepare()
        self.assertFalse(self.handler._get_token.called)
        self.assertFalse(self.handler.check_perms.called)

    @async_test
    def test_async_prepare_with_bad_token(self):
        self.handler.request.headers = {
            'Referer': 'http://nada.com/bla/1.html',
            'Authentication': 'token: bad-token'}

        with self.assertRaises(HTTPError):
            yield from self.handler.async_prepare()

    @async_test
    def test_async_prepare_with_malformed_token(self):
        self.handler.request.headers = {
            'Referer': 'http://nada.com/bla/1.html',
            'Authentication': 'token:'}

        with self.assertRaises(HTTPError):
            yield from self.handler.async_prepare()

    @async_test
    def test_async_prepare_with_token_without_domain_perms(self):
        access_token = AccessToken(name='test-token',
                                   domains=['tudo.com'])
        token = yield from access_token.save()
        self.handler.request.headers = {
            'Referer': 'http://nada.com/bla/1.html',
            'Authentication': 'token: {}'.format(token)}

        with self.assertRaises(HTTPError):
            yield from self.handler.async_prepare()

    @async_test
    def test_async_prepare_ok(self):

        self.access_token = AccessToken(name='test-token')
        token = yield from self.access_token.save()
        self.handler.request.headers = {
            'Referer': 'http://nada.com/bla/1.html',
            'Authorization': 'token: {}'.format(token)}

        yield from self.handler.async_prepare()
        self.assertTrue(self.handler.access_token)


class RestAPIAuthMixinTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.conn = MongoConnection()
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()
        super().tearDownClass()

    def setUp(self):
        super().setUp()

        class Fake(Document):
            pass

        self.fake_cls = Fake
        self.request = Mock()
        self.request.body = {}
        self.request.arguments = {}
        application = Mock()
        application.ui_methods = {}

        class TestAuthHandler(mixins.RestAPIAuthMixin, handlers.RestHandler):
            pass

        handler = TestAuthHandler(application, self.request,
                                  **{'model': self.fake_cls})
        self.handler = handler

    @async_test
    def tearDown(self):
        yield from AccessToken.drop_collection()
        yield from Permission.drop_collection()
        super().tearDown()

    def get_new_ioloop(self):
        return tornado.ioloop.IOLoop.instance()

    @async_test
    def test_async_prepare(self):
        self.access_token = AccessToken(name='test-token')
        token = yield from self.access_token.save()
        yield from Permission.create_perms_to(
            self.access_token, self.fake_cls, 'r')
        application = Mock()
        application.ui_methods = {}
        self.request.method = 'GET'
        self.request.headers = {'Referer': 'http://nada.com',
                                'Authorization': 'token: {}'.format(token)}
        handler = handlers.AuthRestHandler(
            application, self.request, **{'model': self.fake_cls})

        yield from handler.async_prepare()
        self.assertTrue(handler.access_token)

    @async_test
    def test_check_perms_with_no_model_perm(self):
        self.access_token = AccessToken(name='test-token')
        token = yield from self.access_token.save()
        yield from Permission.create_perms_to(self.access_token, self.fake_cls,
                                              'r')
        application = Mock()
        application.ui_methods = {}
        self.request.method = 'PUT'
        self.request.headers = {'Referer': 'http://nada.com'}
        handler = handlers.AuthRestHandler(
            application, self.request, **{'model': self.fake_cls})

        with self.assertRaises(HTTPError):
            yield from handler.check_perms(token=token)

    @async_test
    def test_check_perms_ok(self):
        self.access_token = AccessToken(name='test-token')
        token = yield from self.access_token.save()
        yield from Permission.create_perms_to(self.access_token,
                                         self.fake_cls, 'crud')
        application = Mock()
        application.ui_methods = {}
        self.request.method = 'PUT'
        self.request.headers = {'Referer': 'http://nada.com'}
        handler = handlers.AuthRestHandler(
            application, self.request, **{'model': self.fake_cls})

        access_token = yield from handler.check_perms(token=token)
        self.assertTrue(access_token)
