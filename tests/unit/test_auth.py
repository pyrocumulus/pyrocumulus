# -*- coding: utf-8 -*-

from unittest import TestCase
import tornado
from pyrocumulus import auth
from pyrocumulus.db import MongoConnection
from tests import PYVERSION, async_test


class AccessTokenTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.conn = MongoConnection('pyrodb-{}'.format(PYVERSION))
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()

    def setUp(self):
        super(AccessTokenTest, self).setUp()

        class Fake:
            pass

        self.fake_cls = Fake

    def tearDown(self):
        auth.AccessToken.drop_collection()
        auth.Permission.drop_collection()
        super(AccessTokenTest, self).tearDown()

    @async_test
    def test_create_access_token(self):
        """ Ensure that a AccessToken is create properly with a crypto token
        """
        access_token = auth.AccessToken(name='test-token')

        yield from access_token.save()
        self.assertTrue(access_token.token)

    @async_test
    def test_get_by_token(self):
        access_token = auth.AccessToken(name='test-token')
        token = yield from access_token.save()
        rectoken = yield from auth.AccessToken.get_by_token(token)
        self.assertEqual(rectoken, access_token)

    @async_test
    def test_get_perms(self):
        access_token = auth.AccessToken(name='test-token')
        yield from access_token.save()
        model = self.fake_cls
        perms = 'crud'
        yield from auth.Permission.create_perms_to(access_token, model, perms)
        perms = yield from access_token.get_perms(self.fake_cls)
        self.assertIn('c', perms)
        self.assertEqual(len(perms), 4)

    @async_test
    def test_has_create_perm(self):
        access_token = auth.AccessToken(name='test-token')
        yield from access_token.save()
        model = self.fake_cls
        perms = 'cr'
        yield from auth.Permission.create_perms_to(access_token, model, perms)
        has_perm = yield from access_token.has_create_perm(self.fake_cls)
        self.assertTrue(has_perm)

    @async_test
    def test_has_retrieve_perm(self):
        access_token = auth.AccessToken(name='test-token')
        yield from access_token.save()
        model = self.fake_cls
        perms = 'r'
        yield from auth.Permission.create_perms_to(access_token, model, perms)
        has_perm = yield from access_token.has_retrieve_perm(self.fake_cls)
        self.assertTrue(has_perm)

    @async_test
    def test_has_update_perm(self):
        access_token = auth.AccessToken(name='test-token')
        yield from access_token.save()
        model = self.fake_cls
        perms = 'cu'
        yield from auth.Permission.create_perms_to(access_token, model, perms)
        has_perm = yield from access_token.has_update_perm(self.fake_cls)
        self.assertTrue(has_perm)

    @async_test
    def test_has_delete_perm(self):
        access_token = auth.AccessToken(name='test-token')
        yield from access_token.save()
        model = self.fake_cls
        perms = 'crud'
        yield from auth.Permission.create_perms_to(access_token, model, perms)
        has_perm = yield from access_token.has_delete_perm(self.fake_cls)
        self.assertTrue(has_perm)


class PermissionTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.conn = MongoConnection('pyrodb-{}'.format(PYVERSION))
        cls.conn.connect()

    @classmethod
    def tearDownClass(cls):
        cls.conn.disconnect()

    def setUp(self):
        super(PermissionTest, self).setUp()

        class Fake:
            pass

        self.fake_cls = Fake

    def tearDown(self):
        auth.Permission.drop_collection()
        auth.AccessToken.drop_collection()
        super(PermissionTest, self).tearDown()

    @async_test
    def test_create_perms_to(self):

        access_token = auth.AccessToken(name='test-token')
        yield from access_token.save()
        perms = 'rw'

        perms = yield from auth.Permission.create_perms_to(
            access_token, self.fake_cls, perms)

        self.assertEqual(perms.perms, 'rw')
