#!/bin/bash

# $1 = settings module
# $2 = python_exec
# $3 = tornadoenv

# change env vars and run functional tests.
# Used on buildbot
if [ $1 ]
then
    export PYROCUMULUS_SETTINGS_MODULE=$1;
fi

if [ $2 ]
then
    python_exec=$2
else
    python_exec='python'
fi

if [ $3 ]
then
    tornadoenv='--tornadoenv='$3;
else
    tornadoenv='';
fi

$python_exec setup.py test --test-suite=functional_tests.test_autoapi $tornadoenv
